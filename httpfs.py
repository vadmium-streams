#! /usr/bin/env python3

import sys
from http.client import HTTP_PORT, HTTPS_PORT
from http import HTTPStatus
from socket import create_connection, IPPROTO_TCP, TCP_NODELAY
import selectors
import fuse
import ssl
import urllib.parse
from html.parser import HTMLParser
import hashlib
from errno import ENOENT, EIO
from os import fsencode
import string
import email.utils
from datetime import timezone
from math import inf
from select import select
from contextlib import ExitStack

def main():
    parser = fuse.ArgumentParser("url {mnt,''}", '')
    parser.add_argument('-v',
        action='store_true', help='verbose messages')
    parser.add_argument('--false-size', action='store_true',
        help='report large file size until actual size is read')
    parser.add_argument('--timeout-secs')
    [url, name, args] = parser.parse_args()
    
    url = encode_url(url)
    [scheme, suffix] = url.split('://', 1)
    [host, sep, path] = suffix.partition('/')
    type = fuse.Filesystem.DT_DIR
    if not path:
        default = host
    elif path.endswith('/'):
        default = path[:-1].rsplit('/', 1)[-1]
    else:
        default = path.rsplit('/', 1)[-1]
        type = fuse.Filesystem.DT_REG
    if not name:
        name = urllib.parse.unquote(default)
        print(name)
    
    self = Filesystem(scheme, host, path, name, type, args)
    with self.cleanup:
        self.mount("httpfs", url)
        self.selector = selectors.DefaultSelector()
        self.cleanup.callback(self.selector.close)
        self.select_timeout = None
        self.selector.register(self.fuse, selectors.EVENT_READ,
            self.handle_request)
        while True:
            ready = self.selector.select(self.select_timeout)
            for [key, events] in ready:
                #~ try:
                current = self.selector.get_key(key.fileobj)
                #~ except KeyError:
                    #~ continue
                #~ if not current.events & key.events:
                    #~ continue
                key.data()
            if not ready:
                self.selector.get_key(self.conn).data(EOFError('Timed out'))

class Filesystem(fuse.Filesystem):
    def __init__(self, scheme, host, path, name, type, args):
        super().__init__(name, args)
        self.v = args.v
        self.false_size = args.false_size
        self.timeout_secs = float(args.timeout_secs)
        
        self.scheme = scheme
        self.host = host
        self.nodes = {fuse.ROOT_ID: {'path': path, 'attrs': {'type': type}}}
        self.conn = None
        self.cleanup = ExitStack()
        self.cleanup.callback(self.close)
        if type == fuse.Filesystem.DT_DIR:
            self.make_dir()
            self.root_len = max(path.find('?'), 0)
        else:
            self.make_file()
            if self.false_size:
                self.nodes[fuse.ROOT_ID]['attrs']['size'] = fuse.MAX_SIZE
    
    def close(self):
        try:
            if self.conn is not None:
                self.conn.close()
        finally:
            super().close()
    
    def handle_request(self):
        unique = fuse.Filesystem.handle_request(self)
        if unique is not None:
            self.read_unique = unique
            # Changing events to 0 raises ValueError
            self.selector.unregister(self.fuse)
    
    def statfs(fs):
        return dict(
            blocks=0,
            namelen=len(fs.name),
        )
    
    def getattr(self, node):
        node = self.nodes[node]
        attrs = node['attrs']
        if attrs['type'] == fuse.Filesystem.DT_REG and 'size' not in attrs:
            try:
                if self.conn is None or not self.conn_idle \
                        or select((self.conn,), (), (), 0e0)[0]:
                    raise EOFError()
                self.request(b'HEAD', node['path'])
                buf = Buffer(self.conn.recv_into, 0x10000)
                header = buf.read_until(b'\r\n\r\n')
            except (EOFError, ConnectionError):
                if self.conn is not None:
                    self.conn.close()
                    self.conn = None
                self.open_conn()
                self.request(b'HEAD', node['path'])
                buf = Buffer(self.conn.recv_into, 0x10000)
                header = buf.read_until(b'\r\n\r\n')
            status = get_http_status(header)
            conn = get_list_field(header, b'Connection')
            self.conn_idle = b'close' not in conn
            if status != HTTPStatus.OK:
                raise ValueError(f'HTTP {status} response')
            
            try:
                attrs['size'] = int(get_field(header, b'Content-Length'))
            except ValueError:
                print('HEAD response without Content-Length')
            mtime = get_field(header, b'Last-Modified').decode('ascii')
            mtime = email.utils.parsedate_to_datetime(mtime)
            if mtime.tzinfo is None:
                mtime = mtime.replace(tzinfo=timezone.utc)
            attrs['mtime'] = int(mtime.timestamp())
        return attrs
    
    def readdir(self, dir, offset):
        dir = self.get_dir(self.nodes[dir]).items()
        for [i, [name, node]] in enumerate(dir):
            if i < offset:
                continue
            yield (node, name, self.nodes[node]['attrs']['type'], i + 1)
    
    def lookup(self, dir, name):
        dir = self.get_dir(self.nodes[dir])
        if name not in dir:
            raise OSError(ENOENT, None)
        return dir[name]
    
    def get_dir(self, dir):
        if 'dir' not in dir:
            try:
                if self.conn is None or not self.conn_idle \
                        or select((self.conn,), (), (), 0e0)[0]:
                    raise EOFError()
                self.request(b'GET', dir['path'], b'Accept: text/html\r\n')
                buf = Buffer(self.conn.recv_into, 0x10000)
                header = buf.read_until(b'\r\n\r\n')
            except (EOFError, ConnectionError):
                if self.conn is not None:
                    self.conn.close()
                    self.conn = None
                self.open_conn()
                self.request(b'GET', dir['path'], b'Accept: text/html\r\n')
                buf = Buffer(self.conn.recv_into, 0x10000)
                header = buf.read_until(b'\r\n\r\n')
            status = get_http_status(header)
            if status != HTTPStatus.OK:
                raise ValueError(f'HTTP {status} response')
            ctype = get_field(header, b'Content-Type')
            if ctype.split(b';', 1)[0].strip(b' \t').lower() != b'text/html':
                raise TypeError(repr(ctype))
            index = IndexParser()
            
            encoding = get_list_field(header, b'Transfer-Encoding')
            if tuple(encoding) == (b'chunked',):
                while True:
                    chunk = buf.read_until(b'\r\n').rstrip(b'\r\n')
                    chunk = int(chunk.split(b';', 1)[0], 16)
                    if chunk == 0:
                        break
                    buf.copy(chunk, index.feed)
                    while buf.filled < 2:
                        view = memoryview(buf.buf)[buf.filled:]
                        buf.filled += buf.fill(view)
                    if not buf.buf.startswith(b'\r\n', 0, buf.filled):
                        raise ValueError('Expected CRLF at end of chunk')
                    buf.buf[ : buf.filled - 2] = buf.buf[2 : buf.filled]
                    buf.filled -= 2
                while buf.read_until(b'\r\n') != b'\r\n':
                    pass
            else:
                length = int(get_field(header, b'Content-Length'))
                buf.copy(length, index.feed)
            conn = get_list_field(header, b'Connection')
            self.conn_idle = b'close' not in conn
            index.close()
            
            dir['dir'] = dict()
            for [file, [ntype, url]] in index.files.items():
                url = dir['path'].split('?', 1)[0] + url
                burl = url.encode('ascii')
                if len(url) - self.root_len <= 10:
                    hash = fuse.ROOT_ID
                    place = 1
                    for d in burl[self.root_len:].translate(self.URL_TRANS):
                        hash += d * place
                        place *= 84
                else:
                    hash = hashlib.sha256(burl).digest()
                    hash = int.from_bytes(hash[:64//8], 'big')
                if hash in self.nodes:
                    continue
                dir['dir'][fsencode(file)] = hash
                self.nodes[hash] = {'path': url, 'attrs': {'type': ntype}}
                if ntype == fuse.Filesystem.DT_REG and self.false_size:
                    self.nodes[hash]['attrs']['size'] = fuse.MAX_SIZE
        return dir['dir']
    
    URL_TRANS = bytes.maketrans(
        f"%{string.ascii_letters}{string.digits}-._~!$&'()*+,;=/:?@[]"
        .encode('ascii'), bytes(range(84)) )
    
    def read(self, nodeid, offset, size):
        if offset >= self.nodes[nodeid]['attrs'].get('size', inf):
            return b''
        if self.conn is not None:
            if self.conn_idle and select((self.conn,), (), (), 0e0)[0]:
                self.conn_idle = False
            if not self.conn_idle \
                    and (offset != self.conn_offset or nodeid != self.conn_node):
                self.conn.close()
                self.conn = None
        self.conn_fresh = self.conn is None
        if self.conn_fresh:
            self.open_conn()
        self.offset = offset
        self.buffer = Buffer(None, size)
        if self.conn_idle:
            self.conn_node = nodeid
            self.conn_offset = None
            self.start_recv()
        elif self.read_resp():
            return self.finish_read()
        else:
            self.selector.register(self.conn, selectors.EVENT_READ, self.recv_body)
            self.select_timeout = self.timeout_secs
        return ...
    
    def open_conn(self):
        port = {'http': HTTP_PORT, 'https': HTTPS_PORT}[self.scheme]
        self.conn = create_connection((self.host, port))
        self.conn_idle = True
        self.conn.setsockopt(IPPROTO_TCP, TCP_NODELAY, 1)
        if self.scheme != 'http':
            self.conn = ssl.create_default_context().wrap_socket(self.conn,
                suppress_ragged_eofs=False, server_hostname=self.host)
        self.conn_offset = None
    
    def request(self, method, target, fields=b''):
        self.conn_idle = False
        with self.conn.makefile('wb') as writer:
            writer.write(method)
            writer.write(b' /')
            writer.write(target.encode('ascii'))
            writer.write(b' HTTP/1.1\r\n'
                b'Host: ')
            writer.write(self.host.encode('ascii'))
            writer.write(b'\r\n')
            writer.write(fields)
            writer.write(b'User-Agent: httpfs\r\n'
                b'\r\n')
    
    def start_recv(self):
        range = b'Range: bytes=%d-\r\n' % (self.offset + self.buffer.filled)
        self.request(b'GET', self.nodes[self.conn_node]['path'], range)
        self.resp = Buffer(self.conn.recv_into, 0x10000)
        self.selector.register(self.conn, selectors.EVENT_READ,
            self.recv_header)
        self.select_timeout = self.timeout_secs
    
    def recv_header(self, exc=None):
        try:
            if self.v and not self.resp.filled:
                sys.stderr.write('Starting to receive HTTP response\n')
            searched = max(self.resp.filled - 3, 0)
            if self.recv(self.resp, exc=exc) is None:
                return
            end = self.resp.buf.find(b'\r\n\r\n', searched, self.resp.filled)
            if end < 0:
                assert self.resp.filled < len(self.resp.buf)
                return
            
            header = self.resp.buf[ : end + 2]
            status = get_http_status(header)
            if status == HTTPStatus.REQUESTED_RANGE_NOT_SATISFIABLE:
                self.conn_offset = None
                self.reply_read(b'')
                return
            if status != HTTPStatus.PARTIAL_CONTENT \
                    and (status != HTTPStatus.OK
                        or self.offset + self.buffer.filled > 0):
                msg = header.split(b'\r\n', 1)[0]
                raise Exception(msg.decode('ascii'))
            assert not tuple(get_list_field(header, b'Transfer-Encoding'))
            self.resp_len = int(get_field(header, b'Content-Length'))
            conn = get_list_field(header, b'Connection')
            self.keepalive = b'close' not in conn
            
            del self.resp.buf[self.resp.filled:]
            del self.resp.buf[ : end + 4]
            if not self.read_resp():
                self.selector.modify(self.conn, selectors.EVENT_READ, self.recv_body)
                return
        except Exception as exc:
            sys.excepthook(type(exc), exc, exc.__traceback__)
            self.reply_error(self.read_unique, EIO)
            if self.conn:
                self.selector.unregister(self.conn)
            self.select_timeout = None
            self.selector.register(self.fuse, selectors.EVENT_READ,
                self.handle_request)
            return
        self.reply_read(self.finish_read())
    
    def read_resp(self):
        chunk = min(len(self.buffer.buf) - self.buffer.filled, self.resp_len)
        chunk = self.resp.buf[:chunk]
        del self.resp.buf[:len(chunk)]
        self.buffer.buf[self.buffer.filled : self.buffer.filled + len(chunk)] = chunk
        self.buffer.filled += len(chunk)
        self.resp_len -= len(chunk)
        return self.resp_len == 0 or self.buffer.filled == len(self.buffer.buf)
    
    def recv_body(self, exc=None):
        try:
            self.buffer.fill = self.conn.recv_into
            chunk = self.recv(self.buffer,
                self.buffer.filled + self.resp_len, exc=exc)
            if chunk is None:
                return
            self.resp_len -= chunk
            if self.resp_len > 0 and self.buffer.filled < len(self.buffer.buf):
                return
        except Exception as exc:
            sys.excepthook(type(exc), exc, exc.__traceback__)
            self.reply_error(self.read_unique, EIO)
            if self.conn:
                self.selector.unregister(self.conn)
            self.select_timeout = None
            self.selector.register(self.fuse, selectors.EVENT_READ,
                self.handle_request)
            return
        self.reply_read(self.finish_read())
    
    def recv(self, buf, limit=None, *, exc=None):
        try:
            if exc:
                sys.stderr.write('Receive timed out\n')
                raise exc
            return buf.recv(limit)
        except (ConnectionError, EOFError):
            if self.conn_fresh:
                raise
            self.selector.unregister(self.conn)
            self.conn.close()
            self.conn = None
            self.open_conn()
            self.conn_fresh = True
            self.start_recv()
            return None
    
    def finish_read(self):
        self.conn_offset = self.offset + self.buffer.filled
        if self.resp_len == 0:
            self.conn_idle = self.keepalive
            # Only correct the size once the EOF has been read, otherwise if
            # the kernel discovers the changed attribute, it seems to
            # invalidate is cache of data already read (including readahead)
            self.nodes[self.conn_node]['attrs']['size'] = self.conn_offset
        del self.buffer.buf[self.buffer.filled:]
        return self.buffer.buf
    
    def reply_read(self, buffer):
        self.reply(self.read_unique, buffer)
        
        self.selector.unregister(self.conn)
        self.select_timeout = None
        self.selector.register(self.fuse, selectors.EVENT_READ,
            self.handle_request)

class IndexParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.files = dict()
    
    def feed(self, data):
        return super().feed(data.decode('ascii', 'replace'))
    
    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            for [name, value] in attrs:
                if name != 'href':
                    continue
                if '#' in value:
                    continue
                if value.startswith('/'):
                    continue
                file = value.split('?', 1)[0]
                if file.endswith('/'):
                    file = file[:-1]
                    type = fuse.Filesystem.DT_DIR
                else:
                    type = fuse.Filesystem.DT_REG
                if not frozenset(':/').isdisjoint(file):
                    continue
                file = urllib.parse.unquote(file)
                if file in '..':
                    continue
                self.files[file] = (type, encode_url(value))
        super().handle_starttag(tag, attrs)

def get_http_status(header):
    version = b'HTTP/1.1 '
    if not header.startswith(version):
        raise NotImplementedError('Not a HTTP 1.1 response')
    p = len(version)
    return int(header[p : p + 3])

def get_list_field(header, name):
    try:
        header = get_field(header, name)
    except ValueError:
        return
    for i in header.lower().split(b','):
        i = i.strip(b' \t')
        if i:
            yield i

def get_field(header, name):
    name = b'\r\n%b:' % name.title()
    pos = header.title().index(name)
    pos += len(name)
    return header[pos:].split(b'\r\n')[0].strip(b' \t')

def encode_url(url):
    return urllib.parse.quote(url, safe="%:/?#[]@!$&'()*+,;=")

class Buffer:
    def __init__(self, fill, size):
        self.fill = fill
        self.buf = bytearray(size)
        self.filled = 0
    
    def read_until(self, end):
        search = 0
        while True:
            found = self.buf.find(end, search, self.filled)
            if found >= 0:
                break
            if self.filled == len(self.buf):
                raise ValueError('Excessive field size')
            search = max(self.filled - len(end) + 1, 0)
            self.recv()
        found += len(end)
        result = self.buf[:found]
        self.buf[ : self.filled - found] = self.buf[found:self.filled]
        self.filled -= found
        return result
    
    def copy(self, length, feed):
        while length > self.filled:
            feed(self.buf[:self.filled])
            length -= self.filled
            self.filled = 0
            self.recv()
        
        feed(self.buf[:length])
        self.buf[ : self.filled - length] = self.buf[length:self.filled]
        self.filled -= length
    
    def recv(self, limit=None):
        with memoryview(self.buf) as view, view[self.filled:limit] as view:
            filled = self.fill(view)
        if not filled:
            raise EOFError()
        self.filled += filled
        return filled

if __name__ == '__main__':
    with fuse.handle_termination():
        main()

#! /usr/bin/env python3

from urllib.parse import urlsplit
from base64 import urlsafe_b64decode
from net import request_cached
import json
import subprocess
import urllib.request
from shutil import copyfileobj
import selectors
from shorthand import bitmask
from contextlib import ExitStack
import os
import hmac
from io import SEEK_CUR
from datacopy import TerminalLog, Progress
from io import TextIOWrapper
from enum import Enum
import gzip
from net import header_list

def main(url, start='0', *, v=False, n=False):
    megan = Megan(url)
    API = 'https://g.api.mega.co.nz/'
    if megan.folder is None:
        [url, headers, body] = megan.get_file_request()
    else:
        [url, headers, body] = megan.get_folder_request()
        with ExitStack() as cleanup:
            [header, resp] = request_cached(API + url,
                headers=headers, types=('application/json',),
                data=body, cleanup=cleanup)
            megan.handle_folder_response(header, resp.read())
        
        [megan.key, req] = megan.get_folder_file_request(megan.file)
        [url, headers, body] = req
    with ExitStack() as cleanup:
        [header, resp] = request_cached(API + url,
            headers=headers, types=('application/json',),
            data=body, cleanup=cleanup)
        [K, b] = megan.handle_file_response(header, resp.read())
    print('Encryption key:', K)
    
    print('Filename:', megan.name)
    print('Size: {}'.format(format_size(megan.resp['s'])))
    
    start = int(start, 0)
    if start % 16:
        start -= start % 16
        print('Starting on block boundary at +0x{:X}'.format(start))
    if v:
        print('Download URL:', megan.resp['g'])
    
    print('IV:', megan.key[16:24].hex().upper())
    print('MAC:', megan.key[24:32].hex().upper())
    if n:
        return
    
    req = urllib.request.Request(megan.resp['g'])
    with ExitStack() as cleanup:
        if start:
            name = '{}+0x{:X}'.format(megan.name, start)
            print('Writing to', name)
            req.add_header('Range', 'bytes={}-'.format(start))
            download = cleanup.enter_context(urllib.request.urlopen(req))
            file = cleanup.enter_context(open(name, 'xb'))
            iv = (b & ~0 << 64) + (start // 16 & bitmask(128))
            decryptor = make_encryptor('-aes-128-ctr', '-d', K, iv,
                stdout=file)
            cleanup.enter_context(decryptor)
            file.close()
            copyfileobj(download, decryptor.stdin)
            assert not decryptor.wait()
        else:
            chunk_iv = b & ~0 << 64 | b >> 64
            authenticator = Authenticator(megan.resp['s'], K, chunk_iv)
            cleanup.callback(authenticator.close)
            try:
                file = cleanup.enter_context(open(megan.name, 'xb'))
            except FileExistsError:
                file = cleanup.enter_context(open(megan.name, 'r+b'))
                while True:
                    data = file.read(0x10000)
                    if len(data) < 0x10000:
                        break
                    authenticator.update(data)
                if authenticator.pos + len(data) == megan.resp['s']:
                    authenticator.update(data)
                else:
                    limit = len(data) - len(data) % 16
                    with memoryview(data) as view, view[:limit] as slice:
                        authenticator.update(slice)
                    file.seek(-len(data) % 16, SEEK_CUR)
                    pos = authenticator.pos
                    print('Continuing file from {}'.format(format_size(pos)))
                    req.add_header('Range', 'bytes={}-'.format(pos))
            if authenticator.pos < megan.resp['s']:
                download = cleanup.enter_context(urllib.request.urlopen(req))
                iv = (b & ~0 << 64) + (authenticator.pos // 16 & bitmask(128))
                decryptor = make_encryptor('-aes-128-ctr', '-d', K, iv,
                    stdout=subprocess.PIPE)
                cleanup.enter_context(decryptor)
                selector = cleanup.enter_context(selectors.DefaultSelector())
                
                download_buf = b''
                download_pos = 0x10000
                
                log = TerminalLog()
                cleanup.callback(Progress.close, log)
                progress = Progress(log, megan.resp['s'])
                total_dl = 0
                
                def copy_download():
                    nonlocal download_buf, download_pos
                    try:
                        if download_pos >= len(download_buf):
                            if download_pos < 0x10000:
                                raise EOFError()
                            download_buf = download.read(0x10000)
                            if not download_buf:
                                raise EOFError()
                            download_pos = 0
                    except EOFError:
                        selector.unregister(decryptor.stdin)
                        decryptor.stdin.close()
                    else:
                        with memoryview(download_buf) as view, \
                                view[download_pos:] as rest:
                            n = decryptor.stdin.write(rest)
                        download_pos += n
                        
                        nonlocal total_dl
                        total_dl += n
                        progress.update(total_dl)
                os.set_blocking(decryptor.stdin.fileno(), False)
                selector.register(decryptor.stdin, selectors.EVENT_WRITE,
                    copy_download)
                
                def read_data():
                    data = decryptor.stdout.read(0x10000)
                    if not data:
                        raise EOFError()
                    file.write(data)
                    authenticator.update(data)
                selector.register(decryptor.stdout, selectors.EVENT_READ,
                    read_data)
                
                while authenticator.pos < megan.resp['s']:
                    for [k, events] in selector.select():
                        k.data()
            assert hmac.compare_digest(authenticator.digest(), megan.key[24:])
            print('MAC validated')

class Megan:
    def __init__(self, url):
        split = urlsplit(url)
        assert split.netloc == 'mega.nz'
        assert split.scheme == 'https'
        assert not split.query
        
        if split.path == '/':
            self.folder = None
            assert split.fragment.startswith('!')
            [self.id, self.key] = split.fragment[1:].split('!', 2)
            self.key = base64_decode(self.key, 256)
            return
        
        assert split.path.startswith('/folder/')
        self.folder = split.path[len('/folder/'):]
        self.key = split.fragment.split('/file/', 2)
        if len(self.key) == 1:
            [self.key] = self.key
            self.file = None
        else:
            [self.key, self.file] = self.key
        self.key = base64_decode(self.key, 128).hex()
        return
    
    def get_folder_request(self):
        return make_request('f', {
            'r': 1,  # Recursive
            'ca': 1,  # Cache
        }, f'?n={self.folder}')
    
    def handle_folder_response(self, *folder):
        folder = decode_response(*folder)
        self.nodes = dict()
        for f in folder['f']:
            node = {
                'key': base64_decode(f['k'].split(':', 1)[1]),
                'a': f['a'],
                'type': NodeType(f['t']),
                'parent': f['p'],
                'ts': f['ts'],
            }
            existing = self.nodes.setdefault(f['h'], node)
            assert existing is node
            if node['type'] is NodeType.FILE:
                node['size'] = f['s']
        self.root = folder['f'][0]['h']
        node = self.nodes[self.root]
        [k, b] = split_key(self.decrypt_key(node))
        self.name = decrypt_name(node['a'], k)
    
    def get_folder_file_request(self, file):
        key = self.decrypt_key(self.nodes[file])
        
        args = {
            'n': file,
            'g': 1,  # Include download URL
        }
        return (key, make_request('g', args, f'?n={self.folder}'))
    
    def get_file_request(self):
        args = {
            'p': self.id,
            'g': 1,  # Include download URL
        }
        return make_request('g', args)
    
    def handle_file_response(self, *resp):
        self.resp = decode_response(*resp)
        self.root = None
        self.nodes = {None: {
            'type': NodeType.FILE,
            'a': self.resp['at'],
            'size': self.resp['s'],
        }}
        
        [K, b] = split_key(self.key)
        self.name = decrypt_name(self.resp['at'], K)
        return (K, b)
    
    def decrypt_key(self, node):
        with make_encryptor('-aes-128-ecb', '-d', self.key,
                stdout=subprocess.PIPE) as dec:
            [k, err] = dec.communicate(node['key'])
        assert dec.returncode == 0
        return k

def split_key(key):
    a = int.from_bytes(key[:16], 'big')
    b = int.from_bytes(key[16:], 'big')
    return (format(a ^ b, '032X'), b)

def decrypt_name(a, k):
    dec = make_encryptor('-aes-128-cbc', '-d', k, 0, stdout=subprocess.PIPE)
    with dec:
        [a, err] = dec.communicate(base64_decode(a))
    assert dec.returncode == 0
    assert a.startswith(b'MEGA')
    a = a[4:].rstrip(b'\x00')
    a = json.loads(a.decode('utf-8'))
    return a['n']

class NodeType(Enum):
    FILE = 0
    FOLDER = 1

def make_request(type, args, url=''):
    args['a'] = type
    return ('cs' + url, (
        ('User-Agent', 'megan'),
        ('Content-Type', 'application/json'),
        ('Accept-Encoding', 'gzip, x-gzip'),
    ), json.dumps((args,)).encode('ascii'))

def decode_response(header, resp):
    coded = False
    for code in header_list(header, 'Content-Encoding'):
        if code.lower() in {'gzip', 'x-gzip'}:
            if coded:
                raise TypeError('Recursive gzip encoding')
            resp = gzip.decompress(resp)
            coded = True
        else:
            raise TypeError(f'Unhandled encoding: {code!r}')
    
    resp = json.loads(resp)
    if not isinstance(resp, list):
        error(resp)
    [resp] = resp
    if not isinstance(resp, dict):
        error(resp)
    return resp

ERRORS = {
    -1: 'INTERNAL',
    -2: 'ARGS',
    -3: 'AGAIN: Temporary congestion',
    -4: 'RATELIMIT',
    -5: 'FAILED',
    -6: 'TOOMANY: Connection limit exceeded or terms breached',
    -7: 'RANGE',
    -8: 'EXPIRED',
    -9: 'NOENT',
    -10: 'CIRCULAR',
    -11: 'ACCESS',
    -12: 'EXIST',
    -13: 'INCOMPLETE',
    -14: 'KEY: Decryption failed',
    -15: 'SID: Invalid session',
    -16: 'BLOCKED',
    -17: 'OVERQUOTA',
    -18: 'TEMPUNAVAIL',
    -19: 'TOOMANYCONNECTIONS',
    -20: 'WRITE',
    -21: 'READ',
    -22: 'APPKEY',
}
def error(code):
    raise Exception(f'{ERRORS[code]} ({code})')

def base64_decode(s, bits=None):
    if bits is not None:
        assert len(s) == ceildiv(bits, 6)
    return urlsafe_b64decode(s + '=' * (-len(s) % 4))

class Authenticator(ExitStack):
    def __init__(self, size, K, chunk_iv):
        ExitStack.__init__(self)
        self.size = size
        self.K = K
        self.whole_mac = self.enter_context(CbcMac(self.K))
        self.pos = 0
        self.chunk_size = 0
        self.chunk_mac = None
        self.chunk_cleanup = self.enter_context(ExitStack())
        assert self.size > 0
        self.padding = -self.size % 16
        self.end = self.size + self.padding
        self.chunk_iv = chunk_iv
    
    def update(self, data):
        with memoryview(data) as view:
            pos = 0
            while True:
                if not self.chunk_mac:
                    # New MAC chunk
                    self.chunk_mac = CbcMac(self.K, self.chunk_iv)
                    self.chunk_cleanup.callback(self.chunk_mac.close)
                    if self.chunk_size < 1 << 20:
                        self.chunk_size += 0x20000
                    self.chunk_pos = 0
                chunk_end = pos + min(self.chunk_size - self.chunk_pos,
                    self.end - self.pos)
                with view[pos:max(chunk_end - 16, 0)] as early:
                    self.chunk_mac.write(early)
                    pos += len(early)
                    self.chunk_pos += len(early)
                    self.pos += len(early)
                if pos < chunk_end - 16:
                    break
                block = view[pos:chunk_end]
                self.chunk_mac.write_last_block(block)
                pos += len(block)
                self.chunk_pos += len(block)
                self.pos += len(block)
                if self.pos == self.size:
                    self.chunk_mac.write_last_block(bytes(self.padding))
                    self.whole_mac.write_last_block(self.chunk_mac.get_mac())
                    return
                if pos < chunk_end:
                    break
                self.whole_mac.write(self.chunk_mac.get_mac())
                self.chunk_cleanup.close()
                self.chunk_mac = None
    
    def digest(self):
        mac = int.from_bytes(self.whole_mac.get_mac(), 'big')
        mac ^= mac >> 32
        mac = mac.to_bytes(16, 'big')
        return mac[4:8] + mac[12:16]

class CbcMac(ExitStack):
    def __init__(self, K, iv=0):
        ExitStack.__init__(self)
        self.proc = make_encryptor('-aes-128-cbc', '-e', K, iv,
            stdout=subprocess.PIPE)
        self.enter_context(self.proc)
        self.selector = self.enter_context(selectors.DefaultSelector())
        os.set_blocking(self.proc.stdin.fileno(), False)
        self.selector.register(self.proc.stdin, selectors.EVENT_WRITE,
            self.feed_data)
        self.selector.register(self.proc.stdout, selectors.EVENT_READ,
            self.skip_enc)
        self.to_skip = 0
    
    def write(self, data):
        self.to_skip += len(data)
        self.write_last_block(data)
    
    def feed_data(self):
        with self.data[self.data_pos:] as data:
            self.data_pos += self.proc.stdin.write(data)
    
    def skip_enc(self):
        chunk = min(self.to_skip, 0x10000)
        self.to_skip -= len(self.proc.stdout.read(chunk))
    
    def write_last_block(self, data):
        with memoryview(data) as self.data:
            self.data_pos = 0
            while self.data_pos < len(data):
                for [key, events] in self.selector.select():
                    key.data()
    
    def get_mac(self):
        self.proc.stdin.close()
        while self.to_skip:
            self.skip_enc()
        return self.proc.stdout.readall()

def make_encryptor(cipher, dir, K, iv=None, *, stdout):
    args = ['openssl', 'enc', cipher, '-nopad', dir, '-K', K]
    if iv is not None:
        args.extend(('-iv', format(iv, '032X')))
    return subprocess.Popen(args,
        stdin=subprocess.PIPE, stdout=stdout, bufsize=0)

def format_size(s):
    si = format(s)
    [prefix, scaled_exp] = divmod(len(si) - 1, 3)
    if prefix:
        whole = si[:1 + scaled_exp]
        fraction = si[1 + scaled_exp:]
        prefix = 'kMGTPE'[prefix - 1]
        si = '{}.{} {}B'.format(whole, fraction, prefix)
        prefix = (s.bit_length() - 1) // 10
        bin = s / 1024**prefix
        digits = format(bin, '#.3g')
        if 'e' in digits:
            prefix += 1
            bin /= 1024
            digits = format(bin, '#.3g')
        prefix = 'KMGTPE'[prefix - 1]
        size = '{} ({} {}iB)'.format(si, digits.rstrip('.'), prefix)
    else:
        size = si + ' B'
    return size

def ceildiv(a, b):
    return -(-a // b)

if __name__ == "__main__":
    import clifunc
    clifunc.run()

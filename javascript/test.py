from unittest import TestCase
import javascript
from io import StringIO
from unittest.mock import patch
from math import inf, ldexp, copysign
from itertools import product, combinations, chain

class Test(TestCase):
    def test_identifier_escaped(self):
        for [code, name] in (
            (r'ab\u0063de', 'abcde'),
            (r'\u005Cu123\u0034', '\\u1234'),
        ):
            with self.subTest(code):
                self.assert_statement(code, ('identifier', name))
        for code in ('ab\\y0033z', 'ab\\', '\\u123\\u0034'):
            with self.subTest(code):
                parser = javascript.parse(StringIO(code))
                self.assertRaises(Exception, next, parser, None)
    
    def test_token_buffering(self):
        with patch.object(javascript.Parser, 'BUFSIZE', 10):
            self.assert_statement('abcdefg >>>= x',
                ('>>>=', ('identifier', 'abcdefg'), ('identifier', 'x')),
            )
    
    def test_precedence_non_assign(self):
        pairs = combinations(self.DECREASING_PREC_NON_ASSIGN, 2)
        for [higher, lower] in pairs:
            for [higher, lower] in product(higher, lower):
                with self.subTest(f'{higher} vs {lower}'):
                    self.assert_statement(f'A {lower} B {higher} C',
                        (lower, self.A, (higher, self.B, self.C)))
                    self.assert_statement(f'A {higher} B {lower} C',
                        (lower, (higher, self.A, self.B), self.C))
    
    def test_assign_precedence(self):
        for assign in self.ASSIGNMENTS:
            for op in chain.from_iterable(self.ASSIGN_RHS_ONLY):
                with self.subTest(f'{assign} {op}'):
                    self.assert_statement(f'A {assign} B {op} C',
                        (assign, self.A, (op, self.B, self.C)))
            self.assert_statement(f'A . B {assign} C',
                (assign, ('property', self.A, 'B'), self.C))
            self.assert_statement(f'A, B {assign} C',
                (',', self.A, (assign, self.B, self.C)))
            self.assert_statement(f'A {assign} B, C',
                (',', (assign, self.A, self.B), self.C))
    
    def test_left_associativity(self):
        for set in self.DECREASING_PREC_NON_ASSIGN:
            for [lhs, rhs] in product(set, repeat=2):
                with self.subTest(f'{lhs} {rhs}'):
                    self.assert_statement(f'A {lhs} B {rhs} C',
                        (rhs, (lhs, self.A, self.B), self.C))
    
    def test_right_associativity(self):
        for [lhs, rhs] in product(self.ASSIGNMENTS, repeat=2):
            with self.subTest(f'{lhs} {rhs}'):
                self.assert_statement(f'A {lhs} B {rhs} C',
                    (lhs, self.A, (rhs, self.B, self.C)))
    
    def test_conditional_precedence(self):
        for higher in self.ASSIGN_RHS_ONLY:
            for higher in higher:
                with self.subTest(higher):
                    self.assert_statement(f'1 {higher} 2 ? 3 : 4',
                        ('?', (higher, 1, 2), 3, 4))
                    self.assert_statement(f'1 ? 2 {higher} 3 : 4',
                        ('?', 1, (higher, 2, 3), 4))
                    self.assert_statement(f'1 ? 2 : 3 {higher} 4',
                        ('?', 1, 2, (higher, 3, 4)))
        
        self.assert_statement('1, 2 ? 3 : 4', (',', 1, ('?', 2, 3, 4)))
        self.assert_statement('1 ? 2 : 3, 4', (',', ('?', 1, 2, 3), 4))
        
        for assign in self.ASSIGNMENTS:
            with self.subTest(assign):
                self.assert_statement(f'A {assign} 1 ? 2 : 3',
                    (assign, self.A, ('?', 1, 2, 3)))
                self.assert_statement(f'1 ? A {assign} 2 : 3',
                    ('?', 1, (assign, self.A, 2), 3))
                self.assert_statement(f'1 ? 2 : A {assign} 3',
                    ('?', 1, 2, (assign, self.A, 3)))
        
        self.assert_statement('1 ? 2 : 3 ? 4 : 5',
            ('?', 1, 2, ('?', 3, 4, 5)))
        self.assert_statement('1 ? 2 ? 3 : 4 : 5',
            ('?', 1, ('?', 2, 3, 4), 5))
    
    ASSIGN_RHS_ONLY = (
        ('*', '/', '%'), ('+', '-'),
        ('<<', '>>', '>>>'),
        ('<', '>', '<=', '>=', 'instanceof', 'in'),
        ('==', '!=', '===', '!=='),
        ('&',), ('^',), ('|',), ('&&',), ('||',),
    )
    DECREASING_PREC_NON_ASSIGN = ASSIGN_RHS_ONLY + ((',',),)
    
    ASSIGNMENTS \
        = ('', '*', '/', '%', '+', '-', '<<', '>>', '>>>', '&', '^', '|')
    ASSIGNMENTS = tuple(f'{op}=' for op in ASSIGNMENTS)
    
    A = ('identifier', 'A')
    B = ('identifier', 'B')
    C = ('identifier', 'C')
    
    def test_comment_greed(self):
        self.assert_statement('/* Comment */ 83 /* Comment */', 83)
    
    def test_rounding(self):
        for [code, rounded] in (
            ('0.24703''28229''20623''27208e-323', 0),
            ('0.24703''28229''20623''27209e-323', ldexp(2**-52, -1022)),
            ('24703''28229''20623''27208e-343', 0),
            ('24703''28229''20623''27209e-343', ldexp(2**-52, -1022)),
            ('0.17976''93134''86231''58079e+309', ldexp(1 - 2**-53, +1024)),
            ('0.17976''93134''86231''58080e+309', inf),
            ('17976''93134''86231''58079e+289', ldexp(1 - 2**-53, +1024)),
            ('17976''93134''86231''58080e+289', inf),
            (f'{2**52 + 0}.5', 2**52),
            (f'{2**52 + 1}.5', 2**52 + 2),
            (hex( 2**1024 - 2**(1024 - 53) + (0b10 << (1024 - 55)) ), inf),
            (hex( 2**1024 - 2**(1024 - 53) + (0b01 << (1024 - 55)) ),
                ldexp(1 - 2**-53, +1024)),
            (hex(2**53 + 0b01), 2**53),
            (hex(2**53 + 0b11), 2**53 + 0b100),
        ):
            with self.subTest(code):
                [[labels, code]] = javascript.parse(StringIO(code))
                self.assertEqual(code, rounded)
                self.assertEqual(copysign(1, code), copysign(1, rounded))
    
    def test_div_vs_regex(self):
        regex = ('regex', 'a', 'g')
        def div(x):
            return ('/', ('/', x, ('identifier', 'a')), ('identifier', 'g'))
        
        for [code, ast] in (
            ('/a/g', regex),
            ('x /a/g', div( ('identifier', 'x') )),
            ('{ /a/g }', ('block', (
                (frozenset(), regex),
            ))),
            ('return /a/g', ('return', regex)),
            ('[] /a/g', div([])),
            ('++ /a/g', ('prefix ++', regex)),
            ('\n ++ /a/g', ('prefix ++', regex)),
            ('x ++ /a/g', div( ('postfix ++',  ('identifier', 'x')) )),
            ('++ ++ /a/g', ('prefix ++', ('prefix ++', regex))),
            ('{ } /a/g', regex),
            ('( { } /a/g )', div(('object', []))),
            ('( x ) /a/g', div( ('identifier', 'x') )),
            ('if ( x ) /a/g',
                ('if', ('identifier', 'x'), (frozenset(), regex),
                    (frozenset(), ('block', ())))),
            ('try {} catch (x) {} /a/g', regex),
            ('try {} finally {} /a/g', regex),
        ):
            with self.subTest(code):
                for stmt in javascript.parse(StringIO(code)):
                    pass
                self.assertEqual(stmt, (frozenset(), ast))
    
    def test_array_literal(self):
        ELISION = ('undefined',)
        self.assert_statement('[]', [])
        self.assert_statement('[, ]', [ELISION])
        self.assert_statement('[, , ]', [ELISION, ELISION])
        self.assert_statement('[81]', [81])
        self.assert_statement('[81, ]', [81])
        self.assert_statement('[81, , ]', [81, ELISION])
        self.assert_statement('[81, , , ]', [81, ELISION, ELISION])
        self.assert_statement('[81, 82]', [81, 82])
        self.assert_statement('[81, , 82]', [81, ELISION, 82])
        self.assert_statement('[81, , , 82]', [81, ELISION, ELISION, 82])
    
    def test_object_literal(self):
        self.assert_statement('({})', ('object', []))
        self.assert_statement('({A: 81})', ('object', [('A', 81)]))
        self.assert_statement('({A: 81, B: 82})',
            ('object', [('A', 81), ('B', 82)]))
        self.assert_statement('({"A": 81})', ('object', [('A', 81)]))
        self.assert_statement('({81: 82})', ('object', [(81, 82)]))
    
    def assert_statement(self, code, expected):
        [code] = javascript.parse(StringIO(code))
        self.assertEqual(code, (frozenset(), expected))

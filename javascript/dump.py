from . import parse
from . import OP_PRECEDENCE, UNARY_PRECEDENCE, is_identifier, RESERVED_WORDS
from math import inf, ceil, log10

def dump(reader):
    dump_block(parse(reader))

def dump_block(block, level=0):
    for stmt in block:
        if stmt[0] or not isinstance(stmt[1], tuple) \
                or stmt[1][0] != 'block' or stmt[1][1]:
            dump_stmt(stmt, level)

def dump_stmt(stmt, level):
    [labels, stmt] = stmt
    for label in labels:
        print(f'{(level - 1) * "    "}{label}:')
    print(end=level * '    ')
    if isinstance(stmt, tuple):
        if stmt[0] == 'var':
            dump_var(stmt, level)
        elif stmt[0] in {'return', 'throw'}:
            [stmt, expr] = stmt
            print(end=stmt)
            if expr != ('undefined',):
                print(end=' ')
                dump_expr(expr, level=level)
        elif stmt[0] == 'for':
            [init, test, final, stmt] = stmt[1:]
            if init == final == ('undefined',) and test is not True:
                print(end='while (')
                dump_expr(test, level=level)
            else:
                dump_for(init, level)
                print(end='; ')
                if test is not True:
                    dump_expr(test, level=level)
                print(end='; ')
                if final != ('undefined',):
                    dump_expr(final, level=level)
            dump_substmt(')', stmt, level)
            return
        elif stmt[0] == 'block':
            print('{')
            [stmt] = stmt[1:]
            dump_block(stmt, level + 1)
            print(f'{level * "    "}}}')
            return
        elif stmt[0] == 'if':
            print(end='if (')
            [cond, true, false] = stmt[1:]
            dump_expr(cond, level=level)
            dump_substmt(')', true, level)
            if false[0] or false[1][0] != 'block' or false[1][1]:
                dump_substmt(f'{level * "    "}else', false, level)
            return
        elif stmt[0] in {'break', 'continue'}:
            [stmt, label] = stmt
            print(end=stmt)
            if label is not None:
                print(end=f' {label}')
        elif stmt[0] == 'for in':
            [var, iter, stmt] = stmt[1:]
            dump_for(var, level)
            print(end=' in ')
            dump_expr(iter, level=level)
            dump_substmt(')', stmt, level)
            return
        elif stmt[0] == 'try':
            print('try {')
            [block, exc, catch, finally_block] = stmt[1:]
            dump_block(block, level + 1)
            if exc is not None:
                print(f'{level * "    "}}} catch ({exc}) {{')
                dump_block(catch, level + 1)
            if finally_block or exc is None:
                print(f'{level * "    "}}} finally {{')
                dump_block(finally_block, level + 1)
            print(f'{level * "    "}}}')
            return
        elif stmt[0] == 'function':
            dump_function(stmt, level)
            print()
            return
        elif stmt[0] == 'do':
            [stmt, expr] = stmt[1:]
            dump_substmt('do', stmt, level)
            print(end=f'{level * "    "}while (')
            dump_expr(expr, level=level)
            print(end=')')
        elif stmt[0] == 'switch':
            [expr, cases] = stmt[1:]
            print(end='switch (')
            dump_expr(expr, level=level)
            print(') {')
            for [case, stmts] in cases:
                if case == ('undefined',):
                    print(end=f'{level * "    "}default')
                else:
                    print(end=f'{level * "    "}case ')
                    dump_expr(case, level=level)
                print(':')
                dump_block(stmts, level + 1)
            print(f'{level * "    "}}}')
            return
        else:
            dump_expr(stmt, disallow={'function', 'object'}, level=level)
    else:
        dump_expr(stmt, disallow={'function', 'object'}, level=level)
    print(';')

def dump_substmt(code, stmt, level):
    print(end=code)
    [labels, instruct] = stmt
    if instruct[0] != 'block':
        print()
        dump_stmt(stmt, level + 1)
    elif labels:
        print()
        dump_stmt(stmt, level)
    else:
        print(' {')
        [stmt] = instruct[1:]
        dump_block(stmt, level + 1)
        print(f'{level * "    "}}}')

def dump_for(init, level):
    print(end='for (')
    if isinstance(init, tuple) and init[:1] == ('var',):
        dump_var(init, level, disallow={'in'})
    elif init != ('undefined',):
        dump_expr(init, level=level, disallow={'in'})

def dump_var(stmt, level, *, disallow=frozenset()):
    print(end='var ')
    [vars] = stmt[1:]
    for [i, [name, expr]] in enumerate(vars):
        if i > 0:
            print(end=', ')
        print(end=name)
        if expr != ('undefined',):
            print(end=' = ')
            dump_expr(expr, OP_PRECEDENCE[','], level=level)

def dump_expr(expr, precedence=-inf, *,
        disallow=frozenset(), level, disallow_nullary_new=False):
    if isinstance(expr, float):
        print(end=format(expr, f'.{ceil(log10(2) * 52) + 1}g'))
    elif isinstance(expr, str):
        print(end=repr(expr))
    elif isinstance(expr, list):
        print(end='[')
        for [i, elem] in enumerate(expr):
            if elem == ('undefined',):
                if i == 0:
                    print(end=' ')
            else:
                dump_expr(elem, OP_PRECEDENCE[','], level=level)
            if i < len(expr) - 1 or elem == ('undefined',):
                print(end=',')
                if i < len(expr) - 1:
                    print(end=' ')
        print(end=']')
    elif isinstance(expr, tuple):
        if expr[0].startswith('prefix '):
            p = UNARY_PRECEDENCE
        else:
            p = OP_PRECEDENCE.get(expr[0], +inf)
        if p <= precedence or expr[0] in disallow \
                or disallow_nullary_new and expr[0] == 'new' and not expr[2]:
            print(end='(')
            precedence = -inf
            disallow = frozenset()
            end = ')'
        else:
            end = ''
        if expr[0] == 'call':
            [expr, args] = expr[1:]
            dump_expr(expr, UNARY_PRECEDENCE, disallow=disallow, level=level,
                disallow_nullary_new=True)
            dump_args(args, level)
        elif expr[0] == 'function':
            dump_function(expr, level)
        elif expr[0] == 'object':
            [items] = expr[1:]
            print(end='{')
            for [i, [name, expr]] in enumerate(items):
                if i > 0:
                    print(end=', ')
                if is_unreserved_ident(name):
                    print(end=name)
                else:
                    dump_expr(name, level=level)
                print(end=': ')
                dump_expr(expr, OP_PRECEDENCE[','], level=level)
            print(end='}')
        elif expr[0] == 'this':
            print(end='this')
        elif expr[0] in OP_PRECEDENCE:
            rhs = OP_PRECEDENCE[expr[0]]
            if rhs == OP_PRECEDENCE['=']:
                lhs = rhs
                rhs = lhs - 1
            else:
                lhs = rhs - 1
            dump_expr(expr[1], precedence=lhs, disallow=disallow, level=level)
            if expr[0] != ',':
                print(end=' ')
            print(end=f'{expr[0]} ')
            disallow = disallow - {'function', 'object'}
            dump_expr(expr[2], precedence=rhs, disallow=disallow, level=level)
        elif expr[0] == 'identifier':
            [name] = expr[1:]
            print(end=name)
        elif expr[0] == 'property':
            dump_expr(expr[1], UNARY_PRECEDENCE, level=level,
                disallow_nullary_new=True)
            if is_unreserved_ident(expr[2]):
                print(end=f'.{expr[2]}')
            else:
                print(end='[')
                dump_expr(expr[2], level=level)
                print(end=']')
        elif expr[0] == '?':
            dump_expr(expr[1], OP_PRECEDENCE['='],
                disallow=disallow, level=level)
            print(end='? ')
            dump_expr(expr[2], OP_PRECEDENCE[','], level=level)
            print(end=' : ')
            dump_expr(expr[3], OP_PRECEDENCE[','],
                disallow=disallow - {'function', 'object'}, level=level)
        elif expr[0].startswith('prefix '):
            [op, expr] = expr
            if op == 'prefix !' and isinstance(expr, (float, str, bool)):
                print(end=repr(not expr).lower())
            else:
                print(end=op[len('prefix '):])
                if op[len('prefix '):].isalnum():
                    print(end=' ')
                dump_expr(expr, UNARY_PRECEDENCE, level=level)
        elif expr[0].startswith('postfix '):
            dump_expr(expr[1], UNARY_PRECEDENCE, level=level)
            print(end=expr[0][len('postfix '):])
        elif expr[0] == 'new':
            print(end='new ')
            [expr, args] = expr[1:]
            if isinstance(expr, tuple) and expr[0].startswith('postfix '):
                print(end='(')
            dump_expr(expr, UNARY_PRECEDENCE, disallow={'call'}, level=level,
                disallow_nullary_new=args)
            if isinstance(expr, tuple) and expr[0].startswith('postfix '):
                print(end=')')
            if args:
                dump_args(args, level)
        elif expr[0] == 'regex':
            [pattern, flags] = expr[1:]
            print(end=f'/{pattern}/{flags}')
        else:
            raise NotImplementedError(expr)
        print(end=end)
    else:
        LITERALS = {None: 'null', True: 'true', False: 'false'}
        print(end=LITERALS[expr])

def dump_args(args, level):
    print(end='(')
    for [i, expr] in enumerate(args):
        if i > 0:
            print(end=', ')
        dump_expr(expr, OP_PRECEDENCE[','], level=level)
    print(end=')')

def dump_function(f, level):
    [name, params, body] = f[1:]
    if name is None:
        name = ''
    print(end=f'function {name}(')
    for [i, name] in enumerate(params):
        if i > 0:
            print(end=', ')
        print(end=name)
    print(') {')
    dump_block(body, level=level + 1)
    print(end=f'{level * "    "}}}')

def is_unreserved_ident(name):
    return isinstance(name, str) and is_identifier(name) \
        and name not in RESERVED_WORDS

if __name__ == '__main__':
    from sys import stdin
    dump(stdin)

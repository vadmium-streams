from io import StringIO
import re
import math

OP_PRECEDENCE = (
    ('*', '/', '%'), ('+', '-'),
    ('<<', '>>', '>>>'),
    ('<', '>', '<=', '>=', 'instanceof', 'in'),
    ('==', '!=', '===', '!=='),
    ('&',), ('^',), ('|',), ('&&',), ('||',),
    ('=', '*=', '/=', '%=', '+=', '-=',
        '<<=', '>>=', '>>>=', '&=', '^=', '|='),
    (',',),
)
OP_PRECEDENCE = {op: -prec
    for [prec, set] in enumerate(OP_PRECEDENCE)
    for op in set
}
UNARY_PRECEDENCE = +1

RESERVED_WORDS = {
    # Keywords:
    'break', 'else', 'new', 'var', 'case', 'finally', 'return',
    'void', 'catch', 'for', 'switch', 'while', 'continue',
    'function', 'this', 'with', 'default', 'if', 'throw', 'delete',
    'in', 'try', 'do', 'instanceof', 'typeof',
    # Future-reserved words:
    'abstract', 'enum', 'int', 'short', 'boolean', 'export',
    'interface', 'static', 'byte', 'extends', 'long', 'super',
    'char', 'final', 'native', 'synchronized', 'class', 'float',
    'package', 'throws', 'const', 'goto', 'private', 'transient',
    'debugger', 'implements', 'protected', 'volatile', 'double',
    'import', 'public',
    # Literals:
    'null', 'true', 'false',
}

# "$", "_", plus L and Nl categories from UnicodeData-3.0.1.txt
IDENTIFIER_START = r'$_' \
    'A-Za-z\u00AA\u00B5\u00BA\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02B8' \
    '\u02BB-\u02C1\u02D0\u02D1\u02E0-\u02E4\u02EE\u037A\u0386' \
    '\u0388-\u0481\u048C-\u0559\u0561-\u0587\u05D0-\u05F2\u0621-\u064A' \
    '\u0671-\u06D3\u06D5\u06E5-\u06E6\u06FA-\u06FC\u0710\u0712-\u072C' \
    '\u0780-\u07A5\u0905-\u0939\u093D\u0950\u0958-\u0961\u0985-\u09B9' \
    '\u09DC-\u09E1\u09F0-\u09F1\u0A05-\u0A39\u0A59-\u0A5E\u0A72-\u0A74' \
    '\u0A85-\u0AB9\u0ABD\u0AD0-\u0AE0\u0B05-\u0B39\u0B3D\u0B5C-\u0B61' \
    '\u0B85-\u0BB9\u0C05-\u0C39\u0C60-\u0C61\u0C85-\u0CB9\u0CDE-\u0CE1' \
    '\u0D05-\u0D39\u0D60-\u0D61\u0D85-\u0DC6\u0E01-\u0E30\u0E32-\u0E33' \
    '\u0E40-\u0E46\u0E81-\u0EB0\u0EB2-\u0EB3\u0EBD-\u0EC6\u0EDC-\u0F00' \
    '\u0F40-\u0F6A\u0F88-\u0F8B\u1000-\u102A\u1050-\u1055\u10A0-\u10F6' \
    '\u1100-\u135A\u13A0-\u166C\u166F-\u1676\u1681-\u169A\u16A0-\u16EA' \
    '\u1780-\u17B3\u1820-\u18A8\u1E00-\u1FBC\u1FBE\u1FC2-\u1FCC' \
    '\u1FD0-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FFC\u207F\u2102\u2107' \
    '\u210A-\u2113\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D' \
    '\u212F-\u2131\u2133-\u2139\u2160-\u2183\u3005-\u3007\u3021-\u3029' \
    '\u3031-\u3035\u3038-\u303A\u3041-\u3094\u309D-\u30FA\u30FC-\u318E' \
    '\u31A0-\u31B7\u3400-\uA48C\uAC00-\uD7A3\uF900-\uFB1D\uFB1F-\uFB28' \
    '\uFB2A-\uFD3D\uFD50-\uFDFB\uFE70-\uFEFC\uFF21-\uFF3A\uFF41-\uFF5A' \
    '\uFF66-\uFFDC'

# "$" plus L, Nl, Mn, Mc, Nd and Pc (includes "_") categories
IDENTIFIER_PART = r'$\w' \
    '\u00AA\u00B5\u00BA\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02B8' \
    '\u02BB-\u02C1\u02D0-\u02D1\u02E0-\u02E4\u02EE-\u0362\u037A\u0386' \
    '\u0388-\u0481\u0483-\u0486\u048C-\u0559\u0561-\u0587\u0591-\u05BD' \
    '\u05BF\u05C1-\u05C2\u05C4-\u05F2\u0621-\u0669\u0670-\u06D3' \
    '\u06D5-\u06DC\u06DF-\u06E8\u06EA-\u06FC\u0710-\u0963\u0966-\u096F' \
    '\u0981-\u09F1\u0A02-\u0B6F\u0B82-\u0BEF\u0C01-\u0DF3\u0E01-\u0E3A' \
    '\u0E40-\u0E4E\u0E50-\u0E59\u0E81-\u0F00\u0F18-\u0F19\u0F20-\u0F29' \
    '\u0F35\u0F37\u0F39\u0F3E-\u0F84\u0F86-\u0FBC\u0FC6\u1000-\u1049' \
    '\u1050-\u10F6\u1100-\u135A\u1369-\u1371\u13A0-\u166C\u166F-\u1676' \
    '\u1681-\u169A\u16A0-\u16EA\u1780-\u17D3\u17E0-\u17E9\u1810-\u1FBC' \
    '\u1FBE\u1FC2-\u1FCC\u1FD0-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FFC' \
    '\u203F-\u2040\u207F\u20D0-\u20DC\u20E1\u2102\u2107\u210A-\u2113' \
    '\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D\u212F-\u2131' \
    '\u2133-\u2139\u2160-\u2183\u3005-\u3007\u3021-\u302F\u3031-\u3035' \
    '\u3038-\u303A\u3041-\u309A\u309D-\u318E\u31A0-\u31B7\u3400-\uA48C' \
    '\uAC00-\uD7A3\uF900-\uFB28\uFB2A-\uFD3D\uFD50-\uFE23\uFE33-\uFE34' \
    '\uFE4D-\uFE4F\uFE70-\uFEFC\uFF10-\uFF19\uFF21-\uFF3A\uFF3F' \
    '\uFF41-\uFF5A\uFF65-\uFFDC'
IDENTIFIER_PART = fr'[{IDENTIFIER_PART}]|\\u[\da-fA-F]{{4}}'

IDENTIFIER_NAMES = re.compile(fr'''
    ([{IDENTIFIER_START}] | \\u[\da-fA-F]{{4}})
    ({IDENTIFIER_PART})*
''', re.VERBOSE ^ re.ASCII)
is_identifier = IDENTIFIER_NAMES.fullmatch

def parse(reader):
    parser = Parser()
    parser.read = reader.read
    
    parser.REMOVE_Cf = dict()
    for [first, last] in Cf_RANGES:
        for c in range(first, last + 1):
            parser.REMOVE_Cf[c] = None
    
    parser.buffer = ''
    parser.pos = 0
    parser.eof = False
    
    stmts = parser.parse_block(func_defs=True)
    yield next(stmts)  # Ensure at least one statement
    yield from stmts
    assert parser.token is None

Cf_RANGES = (  # From UnicodeData-2.1.9.txt
    (0x200C, 0x200F),
    (0x202A, 0x202E),
    (0x206A, 0x206F),
    (0xFEFF, 0xFEFF),
)

class Parser:
    def parse_block(self, func_defs=False):
        self.read_token(self.parse_regex)
        while self.token not in {None, ('punctuator', '}')}:
            if self.token == ('reserved word', 'function'):
                assert func_defs
                stmt = self.parse_function()
                assert stmt[1] is not None
                yield ((), stmt)
                self.read_token(self.parse_regex)
            else:
                yield self.parse_statement()
    
    def read_token(self, slash=None):
        self.handle_slash = slash
        self.new_line = False
        while True:
            match = Parser.TOKENS.search(self.buffer, self.pos)
            if not match:
                if self.eof:
                    self.token = None
                    break
                self.buffer = str()
                self.fill()
                continue
            self.pos = match.end()
            if self.pos == len(self.buffer) and not self.eof:
                self.buffer = self.buffer[match.start():]
                self.fill()
                continue
            handle = getattr(self, f'handle_{match.lastgroup}')
            self.token = handle(match.group())
            if self.token is not None:
                break
    
    def parse_statement(self):
        labels = set()
        while True:
            if self.token == ('punctuator', '{'):
                stmt = tuple(self.parse_block())
                assert self.token == ('punctuator', '}')
                self.read_token(self.parse_regex)
                return (labels, ('block', stmt))
            elif self.token == ('reserved word', 'return'):
                self.read_token(self.parse_regex)
                stmt = ('return', ('undefined',))
                if not self.new_line and self.token not in \
                        {('punctuator', ';'), ('punctuator', '}'), None}:
                    stmt = ('return', self.parse_expr())
            elif self.token == ('reserved word', 'if'):
                self.read_token()
                assert self.token == ('punctuator', '(')
                
                self.read_token(self.parse_regex)
                expr = self.parse_expr()
                assert self.token == ('punctuator', ')')
                
                self.read_token(self.parse_regex)
                if_stmt = self.parse_statement()
                if self.token == ('reserved word', 'else'):
                    self.read_token(self.parse_regex)
                    else_stmt = self.parse_statement()
                else:
                    else_stmt = (frozenset(), ('block', ()))
                return (labels, ('if', expr, if_stmt, else_stmt))
            elif self.token == ('reserved word', 'var'):
                stmt = self.parse_var()
            elif self.token == ('reserved word', 'try'):
                block = self.parse_curly_block()
                if self.token == ('reserved word', 'catch'):
                    self.read_token()
                    assert self.token == ('punctuator', '(')
                    
                    self.read_token()
                    [type, id] = self.token
                    assert type == 'identifier'
                    
                    self.read_token()
                    assert self.token == ('punctuator', ')')
                    
                    catch = self.parse_curly_block()
                else:
                    id = None
                    catch = ()
                
                if self.token == ('reserved word', 'finally'):
                    finally_block = self.parse_curly_block()
                else:
                    assert catch != ()
                    finally_block = []
                return (labels, ('try', block, id, catch, finally_block))
            elif self.token == ('reserved word', 'for'):
                self.read_token()
                assert self.token == ('punctuator', '(')
                
                self.read_token(self.parse_regex)
                if self.token == ('reserved word', 'var'):
                    init = self.parse_var(disallow='in')
                elif self.token == ('punctuator', ';'):
                    init = ('undefined',)
                else:
                    init = self.parse_expr(disallow='in')
                if self.token == ('punctuator', ';'):
                    self.read_token(self.parse_regex)
                    if self.token == ('punctuator', ';'):
                        test = True
                    else:
                        test = self.parse_expr()
                        assert self.token == ('punctuator', ';')
                    
                    self.read_token(self.parse_regex)
                    if self.token == ('punctuator', ')'):
                        inc = ('undefined',)
                    else:
                        inc = self.parse_expr()
                    stmt = ('for', init, test, inc)
                else:
                    assert self.token == ('reserved word', 'in')
                    self.read_token(self.parse_regex)
                    expr = self.parse_expr()
                    stmt = ('for in', init, expr)
                assert self.token == ('punctuator', ')')
                
                self.read_token(self.parse_regex)
                body = self.parse_statement()
                return (labels, (*stmt, body))
            elif self.token == ('reserved word', 'throw'):
                self.read_token(self.parse_regex)
                assert not self.new_line
                stmt = ('throw', self.parse_expr())
            elif self.token in {('reserved word', 'break'),
                    ('reserved word', 'continue')}:
                stmt = self.token[1]
                self.read_token(self.parse_regex)
                id = None
                if not self.new_line and self.token not in \
                        {('punctuator', ';'), ('punctuator', '}'), None}:
                    id = self.parse_expr()
                    assert isinstance(id, tuple) and id[0] == 'identifier'
                    id = id[1]
                stmt = (stmt, id)
            elif self.token == ('reserved word', 'do'):
                self.read_token(self.parse_regex)
                stmt = self.parse_statement()
                assert self.token == ('reserved word', 'while')
                self.read_token()
                assert self.token == ('punctuator', '(')
                self.read_token(self.parse_regex)
                stmt = ('do', stmt, self.parse_expr())
                assert self.token == ('punctuator', ')')
                self.read_token(self.parse_regex)
            elif self.token == ('reserved word', 'switch'):
                self.read_token()
                assert self.token == ('punctuator', '(')
                self.read_token(self.parse_regex)
                expr = self.parse_expr()
                assert self.token == ('punctuator', ')')
                self.read_token()
                assert self.token == ('punctuator', '{')
                default = False
                cases = list()
                self.read_token()
                while self.token != ('punctuator', '}'):
                    if self.token == ('reserved word', 'case'):
                        self.read_token(self.parse_regex)
                        case = self.parse_expr()
                    else:
                        assert self.token == ('reserved word', 'default')
                        assert not default
                        case = ('undefined',)
                        self.read_token()
                        default = True
                    assert self.token == ('punctuator', ':')
                    self.read_token()
                    stmts = list()
                    while self.token not in {
                        ('reserved word', 'case'),
                        ('reserved word', 'default'),
                        ('punctuator', '}'),
                    }:
                        stmts.append(self.parse_statement())
                    cases.append((case, stmts))
                self.read_token(self.parse_regex)
                return (labels, ('switch', expr, cases))
            elif self.token == ('punctuator', ';'):
                stmt = ('block', ())
            else:
                assert self.token != ('reserved word', 'function')
                stmt = self.parse_expr()
                if self.token == ('punctuator', ':'):
                    assert isinstance(stmt, tuple) and stmt[0] == 'identifier'
                    assert stmt[1] not in labels
                    labels.add(stmt[1])
                    self.read_token(self.parse_regex)
                    continue
            break
        if self.token == ('punctuator', ';'):
            self.read_token(self.parse_regex)
        else:
            assert self.new_line or self.token in {('punctuator', '}'), None}
        return (labels, stmt)
    
    def parse_var(self, **kw):
        vars = list()
        while True:
            self.read_token()
            [type, id] = self.token
            assert type == 'identifier'
            
            self.read_token()
            if self.token == ('punctuator', '='):
                self.read_token(self.parse_regex)
                value = self.parse_expr(self.COMMA_PRECEDENCE, **kw)
            else:
                value = ('undefined',)
            vars.append((id, value))
            if self.token != ('punctuator', ','):
                break
        return ('var', vars)
    
    def parse_curly_block(self):
        self.read_token()
        assert self.token == ('punctuator', '{')
        
        block = list(self.parse_block())
        assert self.token == ('punctuator', '}')
        
        self.read_token(self.parse_regex)
        return block
    
    def parse_expr(self, precedence=-math.inf, *,
            disallow=None, disallow_call=False):
        if self.token == ('reserved word', 'new'):
            self.read_token(self.parse_regex)
            expr = self.parse_expr(precedence=+math.inf, disallow_call=True)
            if self.token == ('punctuator', '('):
                expr = ('new', expr, self.parse_args())
                self.read_token(self.parse_div)
            else:
                expr = ('new', expr, ())
        elif self.token in {
            ('reserved word', 'delete'), ('reserved word', 'void'),
            ('reserved word', 'typeof'),
            ('punctuator', '++'), ('punctuator', '--'),
            ('punctuator', '+'), ('punctuator', '-'),
            ('punctuator', '~'), ('punctuator', '!'),
        }:
            expr = f'prefix {self.token[1]}'
            self.read_token(self.parse_regex)
            expr = (expr, self.parse_expr(precedence=UNARY_PRECEDENCE))
        else:
            if self.token == ('punctuator', '('):
                self.read_token(self.parse_regex)
                expr = self.parse_expr()
                assert self.token == ('punctuator', ')')
            elif self.token == ('punctuator', '['):
                expr = list()
                while True:
                    self.read_token(self.parse_regex)
                    if self.token == ('punctuator', ']'):
                        break
                    if self.token == ('punctuator', ','):
                        expr.append(('undefined',))
                        continue
                    expr.append(self.parse_expr(self.COMMA_PRECEDENCE))
                    if self.token != ('punctuator', ','):
                        assert self.token == ('punctuator', ']')
                        break
            elif self.token == ('reserved word', 'function'):
                expr = self.parse_function()
            elif self.token == ('punctuator', '{'):
                expr = list()
                self.read_token()
                if self.token != ('punctuator', '}'):
                    while True:
                        name = self.token
                        if not isinstance(name, (float, str)):
                            assert isinstance(name, tuple)
                            [token, name] = name
                            assert token in {'identifier', 'reserved word'}
                        
                        self.read_token()
                        assert self.token == ('punctuator', ':')
                        
                        self.read_token(self.parse_regex)
                        value = self.parse_expr(self.COMMA_PRECEDENCE)
                        expr.append((name, value))
                        
                        if self.token != ('punctuator', ','):
                            break
                        self.read_token()
                    assert self.token == ('punctuator', '}')
                expr = ('object', expr)
            elif isinstance(self.token, (float, str)) \
                    or self.token[0] in {'identifier', 'regex'}:
                expr = self.token
            else:
                LITERALS = {
                    ('reserved word', 'null'): None,
                    ('reserved word', 'true'): True,
                    ('reserved word', 'false'): False,
                    ('reserved word', 'this'): ('this',),
                }
                expr = LITERALS[self.token]
            self.read_token(self.parse_div)
        while True:
            if self.token == ('punctuator', '.'):
                self.read_token()
                [type, member] = self.token
                assert type in {'identifier', 'reserved word'}
                expr = ('property', expr, member)
            elif self.token == ('punctuator', '(') and not disallow_call:
                expr = ('call', expr, self.parse_args())
            elif self.token == ('punctuator', '['):
                self.read_token(self.parse_regex)
                expr = ('property', expr, self.parse_expr())
                assert self.token == ('punctuator', ']')
            elif self.token == ('punctuator', '?'):
                if precedence > self.ASSIGN_PRECEDENCE:
                    return expr
                self.read_token(self.parse_regex)
                a = self.parse_expr(self.COMMA_PRECEDENCE)
                assert self.token == ('punctuator', ':')
                self.read_token(self.parse_regex)
                b = self.parse_expr(self.COMMA_PRECEDENCE, disallow=disallow)
                expr = ('?', expr, a, b)
                continue
            elif not self.new_line \
                    and self.token in \
                        {('punctuator', '++'), ('punctuator', '--')} \
                    and precedence <= UNARY_PRECEDENCE:
                expr = (f'postfix {self.token[1]}', expr)
            elif self.token is None \
                    or self.token[0] not in {'punctuator', 'reserved word'} \
                    or self.token[1] not in OP_PRECEDENCE \
                    or self.token[1] == disallow:
                return expr
            else:
                op = self.token[1]
                # Assignments are right-associative; others are left-associative
                rhs_precedence = OP_PRECEDENCE[op]
                if rhs_precedence < precedence or rhs_precedence \
                        == precedence != self.ASSIGN_PRECEDENCE:
                    return expr
                self.read_token(self.parse_regex)
                rhs = self.parse_expr(rhs_precedence, disallow=disallow)
                expr = (op, expr, rhs)
                continue
            self.read_token(self.parse_div)
    
    def parse_args(self):
        args = list()
        self.read_token(self.parse_regex)
        if self.token != ('punctuator', ')'):
            while True:
                args.append(self.parse_expr(self.COMMA_PRECEDENCE))
                if self.token == ('punctuator', ')'):
                    break
                assert self.token == ('punctuator', ',')
                self.read_token(self.parse_regex)
        return args
    
    ASSIGN_PRECEDENCE = OP_PRECEDENCE['=']
    COMMA_PRECEDENCE = OP_PRECEDENCE[',']
    
    def parse_function(self):
        self.read_token()
        if self.token[0] == 'identifier':
            name = self.token[1]
            self.read_token()
        else:
            name = None
        assert self.token == ('punctuator', '(')
        
        params = list()
        self.read_token()
        if self.token != ('punctuator', ')'):
            while True:
                assert self.token[0] == 'identifier'
                params.append(self.token[1])
                
                self.read_token()
                if self.token == ('punctuator', ')'):
                    break
                assert self.token == ('punctuator', ',')
                
                self.read_token()
        
        self.read_token()
        assert self.token == ('punctuator', '{')
        body = list(self.parse_block(func_defs=True))
        assert self.token == ('punctuator', '}')
        return ('function', name, params, body)
    
    BUFSIZE = 0x10000
    
    IDENTIFIER_START_CHAR = re.compile(fr'[{IDENTIFIER_START}]|\\')
    LINE_TERMINATORS = '\n\r\u2028\u2029'
    TOKENS = re.compile(
        fr'(?P<line_terminator> [{LINE_TERMINATORS}])'
        r'| (?P<singleline_comment> //)'
        r'| (?P<multiline_comment> /\*)'
        fr'| (?P<identifier> {IDENTIFIER_START_CHAR.pattern})'
        r'| (?P<number> [\d.])'
        r'| (?P<punctuator>'
            r'\+\+ | -- | && | \|\|'
            r'| (<< | >>>? | [=!]= | [-<>+*%&^|])=?'
            r'| [][{}().;,!~?:=])'
        r'| (?P<slash> /)'
        '| (?P<string> ["\'])'
        # Anything else other than whitespace:
        '| (?P<illegal_codepoint> [^\t\v\f \xA0\u2000-\u200B\u3000])',
    re.DOTALL ^ re.VERBOSE ^ re.ASCII)
    
    def handle_line_terminator(self, token):
        self.new_line = True
    
    LINE_TERMINATOR_PATTERN \
        = re.compile(fr'[{LINE_TERMINATORS}]')
    
    def handle_singleline_comment(self, start):
        self.read_matches(fr'[^{self.LINE_TERMINATORS}]', 1)
    
    def handle_multiline_comment(self, start):
        while True:
            end = self.buffer.find('*/', self.pos)
            if end >= 0:
                break
            if self.LINE_TERMINATOR_PATTERN.search(self.buffer, self.pos):
                self.new_line = True
            if self.eof:
                raise EOFError('Unterminated multi-line comment')
            self.buffer = self.buffer[max(len(self.buffer) - 1, self.pos):]
            self.fill()
        last = self.pos
        self.pos = end + 2
        if self.LINE_TERMINATOR_PATTERN.search(self.buffer, last, end):
            self.new_line = True
    
    def handle_identifier(self, token):
        token += self.read_matches(IDENTIFIER_PART, 6, re.ASCII)
        if token in RESERVED_WORDS:
            return ('reserved word', token)
        token = self.UNICODE_ESCAPES.sub(self.decode_unicode, token)
        assert is_identifier(token)
        assert token not in RESERVED_WORDS
        return ('identifier', token)
    
    UNICODE_ESCAPES = re.compile(r'\\u(.{4})', re.ASCII)
    
    @staticmethod
    def decode_unicode(match):
        return chr(int(match.group(1), 16))
    
    def handle_number(self, token):
        number = None
        if token == '.':
            token += self.read_matches(r'\d', 1, re.ASCII)
            if token == '.':
                return ('punctuator', '.')
        else:
            if token == '0':
                if self.read_match(*self.X):
                    number = self.read_matches(r'[\da-fA-F]', 1, re.ASCII)
                    assert number > ''
                    number = int(number, 16)
                    if number >= 2**1024 - 2**(1024 - 54):
                        number = math.inf
            else:  # Nonzero digit
                token += self.read_matches(r'\d', 1, re.ASCII)
            if number is None and self.read_match(*self.POINT):
                token += '.' + self.read_matches(r'\d', 1, re.ASCII)
        if number is None:
            prefix = self.read_match(*self.EXPONENT_PREFIX)
            if prefix:
                exp = self.read_matches(r'\d', 1, re.ASCII)
                assert exp > ''
                token += prefix.group() + exp
            number = token
        assert not self.read_match(self.IDENTIFIER_START_CHAR, 1)
        return float(number)
    
    X = (re.compile(r'[xX]'), 1)
    POINT = (re.compile(r'\.'), 1)
    EXPONENT_PREFIX = (re.compile(r'[eE][-+]?'), 2)
    
    def handle_punctuator(self, token):
        return ('punctuator', token)
    
    def parse_div(self, token):
        if self.read_match(*self.EQUALS):
            return ('punctuator', '/=')
        return ('punctuator', '/')
    
    EQUALS = (re.compile(r'='), 1)
    
    def parse_regex(self, token):
        self.pos -= len(token) - 1
        pattern = list()
        while True:
            pattern.append(self.read_matches(
                fr'[^{self.LINE_TERMINATORS}{self.BACKSLASH}/[]'
                fr'|\\[^{self.LINE_TERMINATORS}]', 2))
            end = self.read_match(*self.SLASH_OR_CLASS)
            if end.group() != '[':
                break
            pattern.extend(('[', self.read_matches(
                fr'[^{self.LINE_TERMINATORS}\]{self.BACKSLASH}]'
                fr'|\\[^{self.LINE_TERMINATORS}]', 2), ']'))
            end = self.read_match(*self.CLASS_END)
            assert end
        flags = self.read_matches(IDENTIFIER_PART, 6, re.ASCII)
        return ('regex', ''.join(pattern), flags)
    
    SLASH_OR_CLASS = (re.compile(r'[/[]'), 1)
    CLASS_END = (re.compile(r']'), 1)
    
    def handle_string(self, quote):
        s = self.read_matches(
            fr'[^{quote}{self.BACKSLASH}{self.LINE_TERMINATORS}]'
            fr'|\\[^1-9xu{self.LINE_TERMINATORS}]'
            r'|\\x[\da-fA-F]{2}'
            r'|\\u[\da-fA-F]{4}', 6, re.ASCII)
        end = self.read_match(re.compile(quote), 1)
        assert end
        assert not self.INVALID_NUL.search(s)
        return self.ESCAPE_SEQUENCES.sub(self.decode_escape, s)
    
    BACKSLASH = '\\{:03o}'.format(ord('\\'))
    INVALID_NUL = re.compile(r'\\0\d', re.ASCII)
    ESCAPE_SEQUENCES = re.compile(r'\\(x.{2}|u.{4}|.)', re.ASCII)
    
    @staticmethod
    def decode_escape(match):
        match = match.group(1)
        if match.startswith(('x', 'u')):
            c = chr(int(match[1:], 16))
        else:
            escapes = {
                'n': '\n', 'b': '\b', 'f': '\f', 'r': '\r', 't': '\t',
                'v': '\v', '0': '\x00'}
            c = escapes.get(match, match)
        return c
    
    def handle_illegal_codepoint(self, codepoint):
        raise ValueError(f'Illegal codepoint: {codepoint!r}')
    
    def read_matches(self, pattern, itemlen, flags=0):
        pattern = re.compile(r'(?:{})*'.format(pattern), flags)
        result = StringIO()
        while True:
            match = pattern.match(self.buffer, self.pos)
            result.write(match.group())
            self.pos = match.end()
            if len(self.buffer) - self.pos >= itemlen or self.eof:
                break
            self.buffer = self.buffer[self.pos:]
            self.fill()
        return result.getvalue()
    
    def read_match(self, pattern, maxlen):
        while self.pos + maxlen > len(self.buffer) and not self.eof:
            self.buffer = self.buffer[self.pos:]
            self.fill()
        match = pattern.match(self.buffer, self.pos)
        if match:
            self.pos = match.end()
        return match
    
    def fill(self):
        size = self.BUFSIZE - len(self.buffer)
        new = self.read(size)
        self.eof = len(new) < size
        self.buffer += new.translate(self.REMOVE_Cf)
        self.pos = 0

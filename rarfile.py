#! /usr/bin/env python3

from sys import stdin
from binascii import crc32
from shorthand import bitmask
from struct import Struct
from io import SEEK_CUR, SEEK_END, SEEK_SET
import re
from contextlib import ExitStack
from io import BytesIO
from shorthand import read_exactly
from datetime import datetime

HEADER = Struct('<HBHH')
LONG_BLOCK = 0x8000
OPTIONAL_BLOCK = 0x4000
MARKER = 0x72
MAIN = 0x73
FILE = 0x74
TERM = 0x7B
FILE_STRUCT = Struct('< LBL L BcHL')
SPLIT_BEFORE = 1
SPLIT_AFTER = 2
SPLIT_MASK = SPLIT_BEFORE^SPLIT_AFTER

def main(volname=None, *files):
    extract_file = None
    with Archive() as vol_cleanup, ExitStack() as extract_cleanup:
        if volname is None:
            vol_cleanup.vol = stdin.buffer
            vol_cleanup.volname = ''
            vol_cleanup.vol = ForwardReader(vol_cleanup.vol)
        else:
            vol_cleanup.open(volname)
        for vol in vol_cleanup:
            if volname is not None:
                print(vol_cleanup.volname)
            blocks = iter(vol)
            while True:
                print(end='+{}:'.format(vol.vol.tell()))
                try:
                    [type, flags, packed, block] = next(blocks)
                except StopIteration:
                    break
                if type == MARKER:
                    print(' Marker (signature) block')
                    continue
                if type == MAIN:
                    print(' Archive volume main block')
                    assert flags & ~0x100 == 0x11
                    assert block.read() == bytes(6)
                elif type == FILE:
                    print(end=' File;')
                    
                    split = flags & SPLIT_MASK
                    if split:
                        part = {
                            SPLIT_BEFORE: 'last part',
                            SPLIT_AFTER: 'first part',
                            SPLIT_BEFORE^SPLIT_AFTER: 'middle part',
                        }[split]
                        print('', part, end=',')
                    
                    [name, unpacked, crc,
                        mtime, mtime_flags, mtime_frac, exttime] = block
                    print('', repr(name), end=',')
                    
                    dict_size = flags & 0xE0
                    if dict_size == 0xE0:
                        print(' directory')
                    else:
                        if extract_file:
                            assert flags & SPLIT_BEFORE and name == extract_name
                        else:
                            if name in files:
                                assert not flags & SPLIT_BEFORE
                                extract_name = name
                                basename = name[-1]
                                extract_file = open(basename, 'xb')
                                extract_cleanup.callback(extract_file.close)
                                extract_crc = 0
                        if extract_file:
                            while packed > 0:
                                chunk = read_exactly(vol.vol, min(packed, 0x10000))
                                packed -= len(chunk)
                                extract_crc = crc32(chunk, extract_crc)
                                extract_file.write(chunk)
                            assert extract_crc == crc
                            if not flags & SPLIT_AFTER:
                                extract_cleanup.close()
                                extract_file = None
                        print(' CRC', format(crc, '08X'))
                elif type == TERM:
                    print(end=' Archive volume end block;')
                    crc = int.from_bytes(read_exactly(block, 4), 'little')
                    print(' CRC', format(crc, '08X'), end=',')
                    print(' part', vol.part)
                    block.seek(6)
                    assert block.read() == bytes(7)
                else:
                    assert type == 0x7A
                    print(' New sub-block')
                    assert flags == LONG_BLOCK
            if vol.vol.tell() > vol.vol.seek(0, SEEK_END):
                print(' Block truncated at', vol.vol.tell())
            else:
                print(' EOF')
    if extract_file:
        raise SystemExit(basename + 'not completely extracted')

class Archive(ExitStack):
    def open(vol_cleanup, volname):
        vol_cleanup.vol = vol_cleanup.enter_context(open(volname, 'rb'))
        vol_cleanup.volname = volname
    
    def __iter__(vol_cleanup):
        arc = None
        while True:
            vol = Volume(vol_cleanup.vol)
            yield vol
            vol_cleanup.close()
            if not vol.nextvol:
                break
            
            if arc is None:
                match = r'(.+)\.part(0*{})\.rar'.format(1 + vol.part)
                match = re.fullmatch(match, vol_cleanup.volname, re.ASCII^re.DOTALL)
                if not match:
                    raise SystemExit('Cannot determine next volume name')
                [arc, digits] = match.groups()
                digits = len(digits)
            vol.part += 1
            volname = '{}.part{:0{}}.rar'.format(arc, 1 + vol.part, digits)
            vol_cleanup.open(volname)

class Volume:
    def __init__(self, vol):
        self.vol = vol
        self.nextvol = False
    
    def __iter__(self):
        while True:
            header = self.vol.read(7)
            if not header:
                return
            [crc, type, flags, size] = HEADER.unpack(header)
            size -= 7
            assert size >= 0
            block = read_exactly(self.vol, size)
            if type == MARKER:
                assert header.startswith(b'Rar!\x1A') and not size
            else:
                assert crc32(block, crc32(header[2:])) & bitmask(16) == crc
            block = BytesIO(block)
            if flags & LONG_BLOCK:
                packed = int.from_bytes(read_exactly(block, 4), 'little')
            else:
                packed = 0
            
            if type == FILE:
                assert flags & ~(SPLIT_MASK^0xE0) == LONG_BLOCK^0x1000
                
                file = read_exactly(block, FILE_STRUCT.size)
                [unpacked, os, crc, time, version, method,
                    name, attrib] = FILE_STRUCT.unpack(file)
                assert os == 2
                assert version == 20 and method == b'0'
                name = read_exactly(block, name)
                
                tflags = int.from_bytes(read_exactly(block, 2), 'little')
                
                mtime_flags = tflags >> MTIME
                if mtime_flags & TIME_VALID:
                    mtime_frac = read_exactly(block, mtime_flags & 3)
                else:
                    mtime_frac = None
                
                exttime = list()
                shift = MTIME
                for i in range(3):
                    shift -= 4
                    f = tflags >> shift
                    if f & TIME_VALID:
                        f = (read_exactly(block, 4 + (f & 3)), f)
                        exttime.append(f)
                    else:
                        exttime.append(None)
                assert not block.read(1)
                
                dict_size = flags & 0xE0
                if dict_size == 0xE0:
                    assert not packed and not unpacked and not crc
                    assert attrib == 0x10
                else:
                    assert dict_size == 0x20
                    assert attrib & ~0x200 == 0x20
                
                name = name.decode('ascii').split('\\')
                block = (name, unpacked, crc,
                    time, mtime_flags, mtime_frac, exttime)
            elif type == TERM:
                self.nextvol = flags & 1
                assert flags & ~1 == OPTIONAL_BLOCK^0xE
                block.seek(4)
                self.part = int.from_bytes(read_exactly(block, 2), 'little')
                block.seek(0)
            
            end = self.vol.tell() + packed
            yield (type, flags, packed, block)
            self.vol.seek(end)

def unpack_dostime(time):
    time = datetime(
        1980 + (time >> 25), time >> 21 & 15, time >> 16 & 31,
        time >> 11 & 31, time >> 5 & 63, (time & 15) * 2,
    )
    return int(time.timestamp())

def unpack_exttime_field(tflags, fract):
    odd = bool(tflags & 4)
    fract = int.from_bytes(bytes(3 - len(fract)) + fract, 'little')
    return (odd, fract * 100)
MTIME = 12
TIME_VALID = 8

class ForwardReader:
    def __init__(self, reader):
        self.reader = reader
        self.pos = 0
    
    def read(self, n):
        result = self.reader.read(n)
        self.pos += len(result)
        if len(result) < n:
            self.end = self.pos
        return result
    
    def seek(self, pos, base=SEEK_SET):
        try:
            return self.reader.seek(pos, base)
        except OSError:
            if base == SEEK_SET:
                pos -= self.tell()
                base = SEEK_CUR
            if base == SEEK_CUR:
                assert pos >= 0
                while pos:
                    chunk = min(pos, 0x10000)
                    data = self.read(chunk)
                    pos -= len(data)
                    if len(data) < chunk:
                        self.end = self.pos
                        self.pos += pos
                        break
                return self.pos
            assert base == SEEK_END and not pos
            return self.end
    
    def tell(self):
        return self.pos

if __name__ == "__main__":
    import clifunc
    clifunc.run()

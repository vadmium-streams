from sys import stderr, argv
import os#, os.path
from io import UnsupportedOperation
from warnings import warn
import time
from io import SEEK_END
from contextlib import ExitStack
from math import floor, ceil, log10, exp

import csv
from time import strftime

def main(input, output=''):
    #~ output = os.path.join(output, os.path.basename(input))
    with ExitStack() as cleanup:
        input = cleanup.enter_context(open(input, 'rb', 0))
        size = os.fstat(input.fileno()).st_size
        output = cleanup.enter_context(open(output, 'ab'))
        offset = output.seek(0, SEEK_END)
        input.seek(offset)
        log = TerminalLog()
        cleanup.callback(Progress.close, log)
        progress = Progress(log, size, offset)
        while offset < size:
            data = input.read(0x10000)
            if not data:
                raise EOFError('Unexpected end of file')
            output.write(data)
            offset += len(data)
            progress.update(offset)

class TerminalLog:
    def __init__(self):
        # Defaults
        self.tty = False
        self.curses = None
        
        if not stderr:
            return
        try:
            if not stderr.buffer.isatty():
                raise UnsupportedOperation()
        except (AttributeError, UnsupportedOperation):
            return
        
        self.tty = True
        try:
            import curses
        except ImportError:
            return
        self.write(str())
        self.flush()
        termstr = os.getenv("TERM", "")
        fd = stderr.buffer.fileno()
        try:
            curses.setupterm(termstr, fd)
        except curses.error as err:
            warn(err)
        self.curses = curses
    
    def write(self, text):
        if stderr:
            stderr.write(text)
            self.flushed = False
    
    def flush(self):
        if stderr and not self.flushed:
            stderr.flush()
            self.flushed = True
    
    def carriage_return(self):
        if self.curses:
            self.tput("cr")
        elif self.tty:
            self.write("\r")
        else:
            self.write("\n")
    
    def clear_eol(self):
        return self.tput("el")
    
    def tput(self, capname):
        if not self.curses:
            return
        string = self.curses.tigetstr(capname)
        segs = string.split(b"$<")
        string = segs[0]
        string += bytes().join(s.split(b">", 1)[1] for s in segs[1:])
        self.flush()
        stderr.buffer.write(string)

class Progress:
    TIME_CONST = 1
    SAMPLES = 30
    # TODO: try keeping samples for say up to 10 s, but drop samples
    # that are older than 10 s rather than having a fixed # of samples
    
    def __init__(self, log, total, progress=0):
        self.log = log
        self.total = total
        self.last = time.monotonic()
        #~ self.csv = csv.writer(open(strftime('progress.%Y%m%dT%H%M%S.csv'), 'at', encoding='ascii', newline=''))
        #~ self.csv.writerow(('time', 'progress'))
        self.last_progress = progress
        self.last_rate = None
        #~ self.samples = [(self.last, progress)] * self.SAMPLES
        #~ self.sample = 0
        self.rate_prefix = 0
    
    def update(self, progress):
        now = time.monotonic()
        #~ self.csv.writerow((now, progress))
        interval = now - self.last
        if interval < 0.1:
            return
        #~ [then, prev] = self.samples[self.sample]
        rate = (progress - self.last_progress) / interval
        if self.last_rate is not None:
            rate += exp(-interval/self.TIME_CONST) * (self.last_rate - rate)
        self.last = now
        self.last_progress = progress
        self.last_rate = rate
        # TODO: detect non terminal, including IDLE; allow this determination to be overridden
        
        PREFIXES = 'kMGTPE'
        if rate:
            if 0.1 < rate < 10000 * 1e3**len(PREFIXES):
                scaled = round(rate / 1000**self.rate_prefix)
                if 1000 <= scaled < 10000:
                    scaled = format(scaled)
                else:
                    self.rate_prefix = min(int(log10(rate)) // 3, len(PREFIXES))
                    scaled = format(rate / 1000**self.rate_prefix, '#.3g')
                    if 'e' in scaled:
                        if self.rate_prefix < len(PREFIXES):
                            self.rate_prefix += 1
                            scaled = format(rate / 1000**self.rate_prefix, '#.3g')
                        else:
                            scaled = round(rate / 1000**self.rate_prefix)
                            if 1000 <= scaled < 10000:
                                scaled = format(scaled)
                            else:
                                self.rate_prefix = -1
            else:
                self.rate_prefix = -1
            if self.rate_prefix < 0:
                scaled = format(rate, '.0e')
                if 0 < rate < 1:
                    [m, e, expon] = scaled.partition('e')
                    if expon and int(expon) >= -3:
                        scaled = format(rate, '.3f')
                self.rate_prefix = 0
            scaled = scaled.rstrip('.')
            if self.rate_prefix > 0:
                scaled += 'kMGTPE'[self.rate_prefix - 1]
            
            eta = ceil((self.total - progress) / rate)
        else:
            scaled = '0'
            self.rate_prefix = 0
        if rate and eta <= 9999 * 60 + 59:
            [minutes, sec] = divmod(eta, 60)
        else:
            minutes = 9999
            sec = 99
        
        if progress == self.total:
            progress = '100'
        else:
            progress /= self.total
            progress = floor(progress * 100*10)
            [progress, frac] = divmod(progress, 10)
            progress = f'{progress:2}.{frac:01}'
        self.log.carriage_return()
        self.log.clear_eol()
        self.log.write("{:>4}%{:>7}B/s{:>5}m{:02}s".format(
            progress, scaled, f'-{minutes}', sec))
        self.log.flush()
    
    @classmethod
    def close(cls, log):
        log.carriage_return()
        log.clear_eol()

if __name__ == '__main__':
    with open(argv[1], 'rt', encoding='ascii', newline='') as samples:
        samples = csv.reader(samples)
        assert next(samples) == ['time', 'progress']
        samples = tuple(samples)
    total = int(samples[-1][1])
    log = TerminalLog()
    samples = iter(samples)
    import time
    from time import sleep
    [start, sample] = next(samples)
    start = float(start)
    sample = int(sample)
    real_start = time.monotonic()
    progress = Progress(log, total, sample)
    for [t, sample] in samples:
        sample = int(sample)
        t = float(t) - start + real_start - time.monotonic()
        if t > 0:
            sleep(t)
        progress.update(sample)
    log.write('\n')
    #~ main(*argv[1:])

#! /usr/bin/env python3

import sys
from io import TextIOWrapper
from net import header_list
import http.client
import urllib.request
from xml.etree.ElementTree import TreeBuilder
import contextlib
from html.parser import HTMLParser
from streams import DelegateWriter
from shutil import copyfileobj
from gzip import GzipFile
from urllib.parse import urlencode, urljoin, urlsplit
from time import sleep
import json
from datetime import date, time, datetime, timezone
import ssl
from tkinter import Tk, Label, PhotoImage, Entry, StringVar
import javascript
from io import StringIO, BufferedWriter
import re
import hashlib
import os
from base64 import urlsafe_b64encode
import email, email.message, email.generator
import mimetypes
from streams import TeeReader
from socket import socket, SOCK_STREAM, IPPROTO_TCP, SHUT_WR
from socket import getaddrinfo, AI_ADDRCONFIG
from io import BufferedIOBase, UnsupportedOperation, DEFAULT_BUFFER_SIZE
from io import RawIOBase
from math import inf
import selectors

def main(url, *, ciphers=None, no_verify=False):
    cookies = dict()
    conn = HttpClient(url)
    if ciphers is not None or no_verify:
        if ciphers is not None:
            conn.transport.context.set_ciphers(ciphers)
        if no_verify:
            conn.transport.context.check_hostname = False  # Required before CERT_NONE
            conn.transport.context.verify_mode = ssl.CERT_NONE
    with conn:
        handler = ( conn, cookies )
        [location, header, response] = request_html(handler, url)
        if location != url:
            print('Redirection:', location)
        for probe in probes:
            result = probe(response)
            if result:
                break
        else:
            raise SystemExit('No site matched')
        [props, dl] = result
        for prop in props:
            print("{}: {}".format(*prop), file=sys.stderr)
        print(end='', flush=True, file=sys.stderr)
        print(dl(response, location, handler))

probes = list()

@probes.append
def uptobox_probe(response):
    heading = next(response.iter("h1"), None)
    if heading is None:
        return None
    heading = get_text(heading)
    heading = heading.rsplit(" (", 1)
    if len(heading) < 2:
        return None
    [filename, size] = heading
    size = size[:-len(" MB)")]
    return (( ('Filename', filename), ('Size', mib_size(size)) ), uptobox_dl)

def uptobox_dl(response, url, handlers):
    while True:
        dl = response.find(".//td[a]")[0]
        if dl.tag == "a":
            break
        
        input = response.find(".//input[@name='waitingToken']")
        if not input.get("value"):
            time = response.find(".//*[@data-remaining-time]")
            wait(int(time.get("data-remaining-time")))
        field = (input.get("name"), input.get("value"))
        field = urlencode((field,))
        type = ("Content-Type", "application/x-www-form-urlencoded")
        response = request_html(handler, url, field, method="POST",
            headers=(type,),
            data=field.encode("ascii"),
        )
        [location, header, response] = response
    return dl.get("href")

@probes.append
def rapidgator_probe(response):
    btm = response.find(".//div[@class='btm']")
    if btm is None:
        return None
    filename = btm.findtext('.//a').strip()
    size = btm.findtext('div/strong')[:-len(' MB')]
    props = [
        ('Filename', filename),
        ('Size', mib_size(size)),
    ]
    md5 = btm.findtext('.//label')
    if md5 is not None:
        props.append(('MD5', md5[len('MD5: '):]))
    header = response.find(".//*[@id='table_header']").text.strip()
    if header:
        props.append(('Message', header))
    return (props, rapidgator_dl)

def rapidgator_dl(response, url, handler):
    vars = dict.fromkeys((
        'startTimerUrl', 'getDownloadUrl', 'captchaUrl', 'fid', 'secs',
    ))
    for script in response.iter('script'):
        for [name, value] in vars.items():
            if value is not None:
                continue
            value = script.findtext('.').split("var {} = ".format(name), 1)
            if len(value) < 2:
                continue
            [before, value] = value
            [vars[name], after] = value.split(";", 1)
            if vars[name].startswith("'"):
                vars[name] = vars[name][1:-1]
        if all(v is not None for v in vars.values()):
            break
    
    xml_http_request = ('X-Requested-With', 'XMLHttpRequest')
    
    fid = ('fid', vars['fid'])
    timer = vars['startTimerUrl'] or '/download/AjaxStartTimer'
    timer = urljoin( timer, '?' + urlencode((fid,)) )
    [header, sid] = request_json(handler, urljoin(url, timer), timer,
        headers=(xml_http_request,), location=False)
    sid = ('sid', sid['sid'])
    
    wait(int(vars['secs']))
    req = urljoin( vars['getDownloadUrl'], '?' + urlencode((sid,)) )
    [header, body] = request_json(handler, urljoin(url, req), req,
        headers=(xml_http_request,), location=False)  # Sets "sdata__" cookie
    
    captcha = vars['captchaUrl']
    [url, header, captcha] = request_html(handler,
        urljoin(url, captcha), captcha, location=False)
    handler[0].set_quickack()
    
    [rg_form] = captcha.iterfind(".//form[@id='captchaform']")
    frame = rg_form.find(".//noscript//iframe").get('src')
    with HttpClient(frame) as conn:
        client = (conn, dict())
        [frame, header, body] = request_html(client, urllib.parse.urljoin(url, frame), frame,
            location=False)
        
        [form] = body.iter('form')
        img = form.find(".//img[@id='adcopy-puzzle-image']").get('src')
        with contextlib.ExitStack() as cleanup:
            [header, response] = request_cached(client, urljoin(frame, img), img,
                cleanup=cleanup,
                headers=(
                    ("User-Agent", "hostdl"),
                ),
            )
            tk = Tk()
            img = PhotoImage(data=response.read())
            conn.set_quickack()
        Label(tk, image=img).pack()
        response = StringVar()
        entry = Entry(tk, textvariable=response)
        entry.bind('<Return>', lambda *pos, **kw: tk.destroy())
        entry.pack()
        entry.focus_set()
        tk.mainloop()
        # Keep a reference to "img" alive while displayed
        del img
        
        [smurl, header, dl] = submit_form(form, "adcopy_response", response.get(),
            base=frame, handlers=client, close=True)
    c = form.find(".//input[@id='adcopy_challenge']").get('value')
    dl = submit_form(rg_form, "adcopy_challenge", c,
        base=url, handlers=handler, close=True)
    time = datetime.now(timezone.utc)
    [url, header, dl] = dl
    def get_function():
        for script in dl.iter('script'):
            script = get_text(script)
            if not script:
                continue
            for [labels, stmt] in javascript.parse(StringIO(script)):
                if stmt[0] != 'call':
                    continue
                [stmt, func, arg] = stmt
                if func != ('.',
                    ('call', ('identifier', '$'), [('identifier', 'document')]),
                    'ready',
                ) or len(arg) != 1:
                    continue
                [[arg, name, [], body]] = arg
                if arg != 'function':
                    continue
                for [labels, stmt] in body:
                    if stmt[0] != 'function':
                        continue
                    [stmt, name, [], body] = stmt
                    if name == 'getUrl':
                        return body
    dl = next(dl for [labels, [stmt, dl]] in get_function() if stmt == 'return')
    assert isinstance(dl, str)
    time = time.astimezone().isoformat(" ", "seconds")
    print('Local time:', time, file=sys.stderr)
    [time] = header.get_all('Date', (None,))
    if time is not None:
        print('Server time:', time, file=sys.stderr)
    return dl

# Accept-Ranges: none
# Link did not expire in 52 min
# 50 kB/s (0.4 Mbit/s), or 100 kB/s (0.8 Mbit/s) with account
# No captcha

@probes.append
def filefactory_probe(response):
    info = response.findtext(".//*[@id='file_info']")
    if info is None:
        return None
    [size, uploaded] = info.split(' MB uploaded ', 1)
    return ((
        ('Filename', response.findtext(".//*[@class='file-name']")),
        ('Size', mib_size(size)),
        ('Uploaded', parse_date(uploaded)),
    ), filefactory_dl)

def filefactory_dl(response, url, handlers):
    dl = response.find(".//*[@id='file-download-free-action-start']")
    return dl.get('data-href')

@probes.append
def protected_probe(response):
    if response.find(".//input[@name='CaptchaInputText']") is None:
        return None
    return ((), protected_dl)

def protected_dl(response, url, handlers):
    while True:
        form = response.find(".//*[@class='container body-content']//form")
        text = get_text(form.find(".//*[@class='row']")).strip()
        if text:
            sys.stderr.write(f'{text}\n')
        img = form.find(".//img[@id='CaptchaImage']").get('src')
        with contextlib.ExitStack() as cleanup:
            [header, response] = request_cached(handlers, urljoin(url, img), img,
                cleanup=cleanup,
                headers=(
                    ("User-Agent", "hostdl"),
                ),
            )
            tk = Tk()
            img = PhotoImage(data=response.read())
            handlers[0].set_quickack()
        Label(tk, image=img).pack()
        response = StringVar()
        entry = Entry(tk, textvariable=response)
        entry.bind('<Return>', lambda *pos, **kw: tk.destroy())
        entry.pack()
        entry.focus_set()
        tk.mainloop()
        # Keep a reference to "img" alive while displayed
        del img
        
        [url, header, response] = submit_form(form, "CaptchaInputText", response.get(),
            base=url, handlers=handlers)
        if response.find(".//*[@class='UploadHost']"):
            break
    text = response.find(".//*[@class='Encrypted-folder']")
    text = get_text(text)
    assert text.endswith(']')
    text = text[:-1].strip()
    assert text.endswith('MB')
    [file, MiB] = text[:-2].split('[', 1)
    print(f'Filename: {file.rstrip()}', file=sys.stderr)
    print(f'Size: {mib_size(MiB.strip())}', file=sys.stderr)
    
    for host in response.iterfind(".//*[@class='UploadHost']"):
        img = host.find('.//img').get("src")
        assert img.startswith('/content/images/bigicon/')
        assert img.endswith('.png')
        img = img[len("/content/images/bigicon/"):-len(".png")]
        print(f'Host: {img}', file=sys.stderr)
        if img == 'rapidgator.net':
            slug = host.get('data-slug')
    
    fields = {'link': slug}
    VAR_FIELDS = {'token': 'token', 'Slug': 'folder'}
    for script in response.iter('script'):
        script = get_text(script)
        if not script:
            continue
        for [labels, [stmt, vars]] in javascript.parse(StringIO(script)):
            assert stmt == 'var'
            for [name, value] in vars:
                if name in VAR_FIELDS:
                    assert isinstance(value, str)
                    fields[VAR_FIELDS[name]] = value
    
    url = urljoin(url, '/admin/Main/GetInFo')
    with contextlib.ExitStack() as cleanup:
        [url, header, response] = request_text(handlers, url,
            data=urlencode(fields).encode('ascii'),
            cleanup=cleanup,
            location=False,
            close=True,
        )
        dl = response.read()
    assert dl.startswith('redirect: '), dl
    return dl[len('redirect: '):]

@probes.append
def releasebb_probe(response):
    header = response.find(".//*[@class='entry-header']")
    if header is None:
        return None
    
    [img] = header.iter("img")
    assert img.get('src') == 'http://www.rlsbb.ru/shayan/scene.png'
    
    posted = ".//*[@class='entry-meta entry-meta-header-after']"
    [posted] = response.iterfind(posted)
    [posted, sep, rest] = get_text(posted).lstrip().partition(' in ')
    assert posted.startswith('Posted on ')
    [date, t] = posted[len('Posted on '):].split(' at ', 2)
    if t.endswith(' pm'):
        hour12 = 12
    else:
        assert t.endswith(' am')
        hour12 = 0
    [hour, min] = t[:-3].split(':', 2)
    t = time(hour12 + int(hour), int(min))
    
    return ((
        ( 'Title', get_text( header.find(".//*[@class='entry-title']")) ),
        ( 'Posted', datetime.combine(parse_date(date), t) ),
        ( 'Comments', len(response.find(".//*[@class='commentList']")) ),
    ), releasebb_dl)

def releasebb_dl(response, url, handlers):
    releases = dict()
    for entry in releasebb_entries(response):
        rls = None
        for [text, href] in releasebb_scan(entry):
            text = text.strip()
            if not text and not href:
                continue
            match = re.match(
                r'[?\xA1-\U0010FFFF]* *('
                    r'[A-Za-z0-9]+(?P<punct>[. ])'
                    r'[A-Za-z0-9]+'
                    r'((?P=punct)[A-Za-z0-9]+)*(?! mkv| mp4| avi)'
                    r'[- ]\w+(?=[^\w.]|$)'
                r')',
                text, re.ASCII)
            if match:
                rls = re.sub(r'[- .]', ' ', match.group(1)).lower()
            if rls:
                if href is not None and not href[0].startswith((
                    'https://earn4files.com', 'https://4downfiles.org',
                )):
                    releases.setdefault(rls, list()).append(href)
            else:
                releases.setdefault(None, list()).append((text, href))
    
    for [rls, entries] in releases.items():
        if rls is None:
            for [text, href] in entries:
                if not href and text and not text.lstrip(text[0]) and not text.isalnum():
                    continue
                if text:
                    print(end=text)
                if href:
                    if text:
                        print(end=' ')
                    [href, label] = href
                    if label:
                        print(label, end=' ')
                    print(href)
                print()
            continue
        print(rls)
        for [href, label] in entries:
            print('  ', end=href)
            if label:
                print('  ', end=label)
            print()
    
    for rls in releases.keys():
        print(rls)

def releasebb_scan(entry):
    last = yield from releasebb_scan_lines(entry)
    yield (last, None)

def releasebb_scan_lines(entry, line=str()):
    line += entry.text or ''
    for element in entry:
        if element.tag in {'div', 'p'}:
            yield (line, None)
            yield ((yield from releasebb_scan_lines(element)), None)
            assert not (element.tail or '').strip()
        elif element.tag in {'img', 'strong', 'span', 'b'}:
            line = yield from releasebb_scan_lines(element, line)
            line += element.tail or ''
        elif element.tag == 'a':
            assert not len(element)
            label = element.text
            if label == element.get('href'):
                label = None
            yield (line, (element.get('href'), label))
            line = element.tail or ''
        else:
            assert element.tag == 'br'
            assert not len(element) and not (element.text or '')
            yield (line, None)
            line = element.tail or ''
    return line

def releasebb_entries(response):
    [entry] = response.iterfind(".//*[@class='entry-content']")
    yield entry
    yield from \
        response.iterfind(".//*[@class='commentList']//*[@class='content']")

def wait(time):
    sys.stderr.write("Waiting ")
    [mins, secs] = divmod(time, 60)
    if mins > 0:
        [hrs, mins] = divmod(mins, 60)
        if hrs > 0:
            sys.stderr.write(format(hrs) + "h ")
        sys.stderr.write(format(mins) + "m ")
    print(secs, end="s", flush=True, file=sys.stderr)
    sleep(time)
    print(flush=True, file=sys.stderr)

def mib_size(size):
    places = len(size.partition('.')[2])
    return '{} MiB ({:.{}f} MB)'.format(size, float(size) * 1.024**2, places)

def parse_date(uploaded):
    [month, uploaded] = uploaded.split(' ', 1)
    [day, year] = uploaded.split(', ')
    MONTHS = ('January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December')
    if day.endswith(('st', 'nd', 'rd', 'th')):
        day = day[:-2]
    return date(int(year), 1 + MONTHS.index(month), int(day))

def submit_form(form, name, value, *, base, handlers, **kw):
    action = urljoin(base, form.get('action', ""))
    assert form.get('method') == "post"
    
    fields = [(name, value)]
    for input in form.iterfind(".//input[@name]"):
        if input.get('name') != name:
            fields.append( (input.get('name'), input.get('value') or '') )
    return request_html(handlers, action, urlencode(((name, value),)),
        data=urlencode(fields).encode('ascii'),
        location=False,
    **kw)

def request_decoded(conn, url, *pos, headers=(), location=True, **kw):
    headers += (
        ("Accept-Encoding", "gzip, x-gzip"),
        ("User-Agent", "hostdl"),
    )
    method = kw.get('method')
    if not method:
        method = 'GET' if kw.get('data') is None else 'POST'
    redirects = iter(range(location and method in {'GET', 'HEAD'}))
    while True:
        [header, response] = request_cached(conn, url, *pos,
            method=method, headers=headers,
            **kw)
        [status] = header.get_all('Status')
        if not status.startswith('301 '):
            break
        next(redirects)
        [url] = header.get_all('Location')
    
    for encoding in header_list(header, "Content-Encoding"):
        if encoding.lower() in {"gzip", "x-gzip"}:
            if isinstance(response, GzipFile):
                raise TypeError("Recursive gzip encoding")
            response = GzipFile(fileobj=response, mode="rb")
        else:
            msg = "Unhandled encoding: " + repr(encoding)
            raise TypeError(msg)
    return (url, header, response)

def request_text(*pos, **kw):
    [url, header, response] = request_decoded(*pos, **kw)
    try:
        charset = header.get_content_charset()
        return (url, header, TextIOWrapper(response, charset, errors='replace'))
    except:
        response.close()
        raise

def request_html(handler, url, *pos, **kw):
    with contextlib.ExitStack() as cleanup:
        [url, header, response] = request_text(handler, url, *pos,
            #~ types=('text/html',), cleanup=cleanup, **kw)
            cleanup=cleanup, **kw)
        parser = HtmlTreeParser()
        # TODO: limit data
        copyfileobj(response, DelegateWriter(parser.feed))
    return (url, header, parser.close())

def request_json(handler, url, *pos, **kw):
    with contextlib.ExitStack() as cleanup:
        [url, header, response] = request_decoded(handler,
            url, *pos, cleanup=cleanup, **kw)
        with TextIOWrapper(response, 'UTF-8') as response:
            return (header, json.load(response))

def request_cached(client, url, msg=None, *,
        cleanup, method=None, data=None, headers=(), **kw):
    if msg is None:
        msg = url
    request = urllib.request.Request(url, data, dict(headers), method=method)
    request.add_header('Host', request.host)
    if data is not None:
        request.add_header('Content-Length', format(len(data)))
    if request.get_method() == 'POST':
        type = 'application/x-www-form-urlencoded'
        request.add_header('Content-Type', type)
    
    [conn, cookies] = client
    if cookies:
        header = (f'{key}={value}' for [key, value] in cookies.items())
        request.add_header('Cookie', '; '.join(header))
    print(request.get_method(), msg, end=" ", flush=True, file=sys.stderr)
    
    try:
        [dir, sep, nondir] = url[:100].rpartition("/")
        suffix = hashlib.md5()
        if request.get_method() not in {'GET', 'HEAD'}:
            suffix.update(request.get_method().encode('ascii'))
        suffix.update(url.encode())
        if data is not None:
            suffix.update(data)
        suffix = suffix.digest()[:6]
        suffix = urlsafe_b64encode(suffix).decode("ascii")
        print(suffix, end=' ', file=sys.stderr)
        
        if nondir:
            suffix = nondir + os.extsep + suffix
        metadata = f'{dir}{sep}{suffix}{os.extsep}mime'
        metadata = open(metadata, "rb")
    except FileNotFoundError:
        response = conn.getresponse(request, **kw)
        [status, reason, header, response] = response
        print(status, reason, flush=True, file=sys.stderr)
        
        for field in header_list(header, "Connection"):
            del header[field]
        for field in (
            "Close", "Connection", "Keep-Alive",
            "Proxy-Authenticate", "Proxy-Authorization",
            "Public",
            "Transfer-Encoding", "TE", "Trailer",
            "Upgrade",
        ):
            del header[field]
        
        default = (('application/octet-stream', None),)
        [type, value] = header.get_params(default)[0]
        if type != 'application/octet-stream':
            ext = {
                'text/html': 'html', 'text/javascript': 'js',
                'application/json': 'json',
                'audio/mpeg': 'mpga',
                'image/jpeg': 'jpeg',
            }.get(type)
            if ext is None:
                suffix += mimetypes.guess_extension(type, strict=False)
            else:
                suffix += os.extsep + ext
        for encoding in header_list(header, "Content-Encoding"):
            if encoding.lower() in {"gzip", "x-gzip"}:
                suffix += os.extsep + "gz"
                break
        os.makedirs(dir, exist_ok=True)
        cache = open(f'{dir}{sep}{suffix}', "xb")
        cleanup.enter_context(cache)
        msg = email.message.Message()
        msg.add_header("Content-Type",
            "message/external-body; access-type=local-file",
            name=suffix)
        header.add_header('Status',
            '{} {}'.format(status, reason))
        msg.attach(header)
        with open(metadata, "xb") as metadata:
            metadata = email.generator.BytesGenerator(metadata,
                mangle_from_=False, maxheaderlen=0)
            metadata.flatten(msg)
        response = TeeReader(response, cache.write)
    else:
        with metadata:
            msg = email.message_from_binary_file(metadata)
        cache = f'{dir}{sep}{msg.get_param("name")}'
        [header] = msg.get_payload()
        response = cleanup.enter_context(open(cache, "rb"))
        print("(cached)", flush=True, file=sys.stderr)
    for cookie in header.get_all('Set-Cookie', ()):
        assert set('"\\').isdisjoint(cookie)
        [cookie, *attrs] = cookie.split('; ')
        assert ',' not in cookie
        
        expired = False
        for attr in attrs:
            attr = attr.title()
            assert not (attr.startswith('Path=') and attr != 'Path=/')
            if attr.startswith('Max-Age=') \
                    and int(attr[len('Max-Age='):]) <= 0:
                expired = True
        [key, value] = cookie.split('=', 1)
        if expired:
            cookies.pop(key, None)
        else:
            cookies[key] = value
    return (header, response)

class Reader(BufferedIOBase):
    def detatch(self):
        raise UnsupportedOperation()
    
    def readable(self):
        return True
    
    def __next__(self):
        result = self.readline()
        if not result:
            raise StopIteration()
        return result
    
    def __iter__(self):
        return self
    
    def readlines(self, hint=-1):
        result = list()
        total = 0
        for line in self:
            result.append(line)
            total += len(line)
            if hint in range(1, total):
                break
        return result

class HttpClient(contextlib.ExitStack, Reader):
    _buffer_pos = 0
    
    def __init__(self, url):
        contextlib.ExitStack.__init__(self)
        Reader.__init__(self)
        url = urlsplit(url)
        self._buffer = bytearray(DEFAULT_BUFFER_SIZE)
        
        self.transport = globals()[f'Transport_{url.scheme}']()
        self.transport.netloc = url.netloc
        port = getattr(http.client, f'{url.scheme.upper()}_PORT')
        self.transport.port = port
        self.transport.socket = None
        self.transport.conn_cleanup = self.enter_context(contextlib.ExitStack())
        self.enter_context(self.transport)
    
    def getresponse(self, request, close=False):
        assert request.host == self.transport.netloc
        scheme = urlsplit(request.full_url).scheme
        assert type(self.transport) is globals()[f'Transport_{scheme}']
        if self.transport.socket is not None:
            if not self._close_conn:
                self.transport.set_blocking(False)
                try:
                    response = self.read(1)
                except BlockingIOError:
                    self.transport.set_blocking(True)
                    try:
                        self._send_request(request, close=close)
                    except Exception:
                        idempotents = {
                            "GET", "HEAD", "PUT", "DELETE", "TRACE", "OPTIONS"}
                        if request.get_method() not in idempotents \
                                or not self._is_timeout():
                            raise
                    else:
                        if not self._is_timeout():
                            return self._parse_response()
                except Exception:
                    pass
            self.transport.conn_cleanup.pop_all().close()
            self.transport.socket = None
        self.transport.setup()
        self._buffer_filled = 0
        
        self._send_request(request, close=close)
        self._recv_status()
        return self._parse_response()
    
    def _send_request(self, request, close):
        writer = BufferedWriter(RawDelegateWriter(self.transport.send))
        line = f'{request.get_method()} {request.selector} HTTP/1.1\r\n'
        writer.write(line.encode('ascii'))
        for [name, value] in request.header_items():
            writer.write(f"{name}: {value}\r\n".encode('ascii'))
        if close:
            writer.write(b'Connection: close\r\n')
        writer.write(b'\r\n')
        if request.data is not None:
            writer.write(request.data)
        writer.flush()
        if close:
            self.transport.write_end()
        self.transport.flush()
    
    def _is_timeout(self):
        self._recv_status()
        return self._status in {None, http.client.REQUEST_TIMEOUT}
    
    def _recv_status(self):
        while True:
            line = self.readline(1000)
            if line != b'\r\n':
                break
        if b'\r\n'.startswith(line):
            self._status = None
            return
        assert line.endswith(b'\n')
        line = line.rstrip().decode('ascii')
        [version, self._status, self._reason] = line.split(' ', 2)
        self._status = int(self._status)
        assert version == 'HTTP/1.1'
    
    def _parse_response(self):
        msg = http.client.parse_headers(self)
        connection = header_list(msg, 'Connection')
        self._close_conn = 'close' in map(str.lower, connection)
        encodings = header_list(msg, 'Transfer-Encoding')
        encodings = tuple(map(str.lower, encodings))
        if not encodings:
            [length] = msg.get_all('Content-Length')
            reader = LengthReader(self, int(length))
        else:
            assert encodings == ('chunked',)
            reader = ChunkedReader(self)
        return (self._status, self._reason, msg, reader)
    
    def set_quickack(self):
        if self.transport.socket is not None:
            self.transport.set_quickack()
    
    def read(self, size=-1):
        if size is None or size < 0:
            size = inf
        while self._fill_buffer(size):
            pass
        return self._read_buffer(size)
    
    def read1(self, size=-1):
        if size < 0:
            size = 4096
        if self._buffer_filled:
            return self._read_buffer(size)
        result = self.transport.recv(size)
        self.transport.set_delack()
        return result
    
    def readline(self, size=-1):
        if size < 0:
            size = inf
        new = self._buffer_pos
        while True:
            end = self._buffer_pos + min(size, self._buffer_filled)
            end = self._buffer.find(b'\n', new, end)
            if end >= 0:
                size = end + 1 - self._buffer_pos
            new = self._buffer_filled
            if not self._fill_buffer(size):
                break
        return self._read_buffer(size)
    
    def _fill_buffer(self, size):
        if self._buffer_filled >= size:
            return False
        if self._buffer_filled < DEFAULT_BUFFER_SIZE:
            end = self._buffer_pos + self._buffer_filled
            with memoryview(self._buffer) as buffer:
                with buffer[self._buffer_pos:end] as chunk:
                    buffer[:self._buffer_filled] = chunk
                self._buffer_pos = 0
                with buffer[self._buffer_filled:] as buffer:
                    chunk = self.transport.recv_into(buffer)
        else:
            chunk = self.transport.recv(DEFAULT_BUFFER_SIZE)
            self._buffer.extend(chunk)
            chunk = len(chunk)
        self._buffer_filled += chunk
        self.transport.set_delack()
        return chunk
    
    def _read_buffer(self, size):
        if self._buffer_filled > DEFAULT_BUFFER_SIZE:
            result = min(size, self._buffer_filled)
            with memoryview(self._buffer) as buffer, \
                    buffer[:result] as result:
                result = bytes(result)
            del self._buffer[:-DEFAULT_BUFFER_SIZE]
            self._buffer_filled -= len(result)
            self._buffer_pos = DEFAULT_BUFFER_SIZE - self._buffer_filled
            return result
        result = self._buffer_pos
        self._buffer_pos += min(size, self._buffer_filled)
        self._buffer_filled -= self._buffer_pos - result
        with memoryview(self._buffer) as buffer, \
                buffer[result:self._buffer_pos] as result:
            return bytes(result)
    
    def readinto(self, b):
        with memoryview(b) as b, b.cast('B') as b, \
                memoryview(self._buffer) as buffer:
            result = 0
            while True:
                chunk = min(self._buffer_filled, len(b) - result)
                chunk = self._buffer_pos + chunk
                with buffer[self._buffer_pos:chunk] as chunk:
                    b[result : result + len(chunk)] = chunk
                    result += len(chunk)
                    self._buffer_filled -= len(chunk)
                    self._buffer_pos += len(chunk)
                if result == len(b):
                    return result
                self._buffer_pos = 0
                self._buffer_filled = self.transport.recv_into(buffer)
                self.transport.set_delack()
                if not self._buffer_filled:
                    return result
    
    def readinto1(self, b):
        if not self._buffer_filled:
            result = self.transport.recv_into(b)
            self.transport.set_delack()
            return result
        with memoryview(b) as b, b.cast('B') as b, \
                memoryview(self._buffer) as buffer:
            result = self._buffer_pos + min(self._buffer_filled, len(b))
            with buffer[self._buffer_pos:result] as result:
                b[:len(result)] = result
                self._buffer_filled -= len(result)
                self._buffer_pos += len(result)
                return len(result)

def transport_connect(transport, *, delack):
    ai = iter(getaddrinfo(transport.netloc, None,
        type=SOCK_STREAM, flags=AI_ADDRCONFIG))
    args = next(ai)
    try:
        _connect_addr(transport, args, delack=delack)
    except Exception:
        for args in ai:
            try:
                _connect_addr(transport, args, delack=delack)
                break
            except Exception:
                continue
        else:
            raise
    transport.conn_cleanup.enter_context(transport.socket)

def _connect_addr(transport, args, *, delack):
    [family, type, proto, cannonname, sockaddr] = args
    with contextlib.ExitStack() as cleanup:
        transport.socket = socket(family, SOCK_STREAM, proto)
        cleanup.enter_context(transport.socket)
        transport.socket.settimeout(float(10))
        if sys.platform == 'linux':
            transport.socket.setsockopt(IPPROTO_TCP, TCP_QUICKACK,
                not delack)
        sockaddr = (sockaddr[0], transport.port) + sockaddr[2:]
        transport.socket.connect(sockaddr)
        cleanup.pop_all()

class Transport_http(contextlib.ExitStack):
    def setup(self):
        pass
    
    def _connect(self):
        if self.socket is not None:
            return
        
        transport_connect(self, delack=True)
        
        # TODO: recv should connect if necessary, with delack=False
        self.recv = self.socket.recv
        self.recv_into = self.socket.recv_into
        
        if sys.platform == 'linux':
            self.socket.setsockopt(IPPROTO_TCP, TCP_CORK, 1)
    
    def set_blocking(self, blocking):
        self.socket.setblocking(blocking)
    
    def send(self, b):
        self._connect()
        result = self.socket.send(b)
        self.set_delack()
        return result
    
    def write_end(self):
        self._connect()
        self.socket.shutdown(SHUT_WR)
    
    def flush(self):
        if sys.platform == 'linux':
            self.socket.setsockopt(IPPROTO_TCP, TCP_CORK, 0)
            self.socket.setsockopt(IPPROTO_TCP, TCP_CORK, 1)
    
    def set_delack(self):
        if sys.platform == 'linux':
            self.socket.setsockopt(IPPROTO_TCP, TCP_QUICKACK, 0)
    
    def set_quickack(self):
        if sys.platform == 'linux':
            self.socket.setsockopt(IPPROTO_TCP, TCP_QUICKACK, 1)

class Transport_https(contextlib.ExitStack):
    def __init__(self):
        super().__init__()
        self.context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
        self.context.set_alpn_protocols(['http/1.1'])
        self.timeout = float(10)
        self.selector = self.enter_context(selectors.DefaultSelector())
    
    def setup(self):
        self.incoming = ssl.MemoryBIO()
        self.in_ended = False
        self.outgoing = ssl.MemoryBIO()
        self.ssl = self.context.wrap_bio(self.incoming, self.outgoing,
            server_side=False, server_hostname=self.netloc)
        IDLE = type(None)
        self._transfer(IDLE)
        self._transfer(self.ssl.do_handshake)
    
    def _connect(self, *, delack):
        if self.socket is not None:
            return
        transport_connect(self, delack=delack)
        self.socket.setblocking(False)  # Ensure partial writes possible
        self.conn_cleanup.callback(self.teardown)
    
    def teardown(self):
        with contextlib.suppress(KeyError):
            self.selector.unregister(self.socket)
    
    def set_blocking(self, blocking):
        self.timeout = float(10) * blocking
    
    def send(self, b):
        return self._transfer(self.ssl.write, b)
    
    def write_end(self):
        pass
    
    def recv(self, bufsize):
        return self._transfer(self.ssl.read, bufsize)
    
    def recv_into(self, buffer):
        with memoryview(buffer) as view:
            return self._transfer(self.ssl.read, view.nbytes, view)
    
    def _transfer(self, call, *args):
        again = True
        while again:
            want_read = False
            try:
                result = call(*args)
                again = False
            except ssl.SSLWantReadError:
                want_read = True
            except ssl.SSLWantWriteError:
                pass
            out = memoryview(self.outgoing.read())
            out_offset = 0
            shut_pending = self.outgoing.eof
            # Send everything in "outgoing", and if "want_read" is set,
            # make at least one write to "incoming"
            while True:
                events = selectors.EVENT_READ * (
                    self.incoming.pending < DEFAULT_BUFFER_SIZE
                        and not self.in_ended)
                out_pending = out[out_offset:]
                if out_pending or shut_pending:
                    events ^= selectors.EVENT_WRITE
                elif not want_read or self.incoming.pending or self.in_ended:
                    break
                self._connect(delack=events & selectors.EVENT_WRITE)
                self.selector.register(self.socket, events)
                with contextlib.ExitStack() as cleanup:
                    cleanup.callback(self.selector.unregister, self.socket)
                    ready = self.selector.select(self.timeout)
                if not ready:
                    raise BlockingIOError('SSL and TCP transfer timeout')
                for [key, events] in ready:
                    if events & selectors.EVENT_WRITE:
                        if out_pending:
                            out_offset += self.socket.send(out_pending)
                        if out_offset == len(out) and shut_pending:
                            self.socket.shutdown(SHUT_WR)
                            shut_pending = False
                    if events & selectors.EVENT_READ:
                        read = self.socket.recv(
                            DEFAULT_BUFFER_SIZE - self.incoming.pending)
                        self.incoming.write(read)
                        self.in_ended = not read
                        if self.in_ended:
                            self.incoming.write_eof()
        return result
    
    def flush(self):
        ...
    
    def set_delack(self):
        ...
    
    def set_quickack(self):
        ...

class LengthReader(Reader):
    def __init__(self, reader, length):
        super().__init__()
        self._reader = reader
        self._remaining = length
    
    def read(self, size=-1, _method='read'):
        if size not in range(self._remaining):
            size = self._remaining
        if not size:
            return b''
        result = getattr(self._reader, _method)(size)
        if len(result) < size and (_method != 'read1' or not result):
            raise EOFError()
        self._remaining -= len(result)
        return result
    
    def read1(self, *pos, **kw):
        return self.read(*pos, **kw, _method='read1')
    
    def readline(self, *pos, **kw):
        return self.read(*pos, **kw, _method='readline')
    
    def readinto(self, b, _method='readinto'):
        with memoryview(b) as b, b.cast('B') as b, b[:self._remaining] as b:
            if not len(b):
                return 0
            result = getattr(self._reader, _method)(b)
            if result < len(b) and (_method != 'readinto1' or not result):
                raise EOFError()
        self._remaining -= result
        return result
    
    def readinto1(self, *pos, **kw):
        return self.readinto(*pos, **kw, _method='readinto1')

class ChunkedReader(Reader):
    _chunk_remaining = 0
    _chunk_seen = False
    _done = False
    
    def __init__(self, reader):
        super().__init__()
        self._reader = reader
    
    def read(self, size=-1):
        if size is None or size < 0:
            size = inf
        result = bytearray()
        while size > 0 and not self._done:
            if not self._chunk_remaining:
                self._next_chunk()
                if self._done:
                    break
            
            limit = min(size, self._chunk_remaining, DEFAULT_BUFFER_SIZE)
            chunk = self._reader.read(limit)
            if len(chunk) < limit:
                raise EOFError()
            
            self._chunk_remaining -= len(chunk)
            result.extend(chunk)
            size -= len(chunk)
        return bytes(result)
    
    def read1(self, size=-1):
        if not self._chunk_remaining and not self._done:
            self._next_chunk()
        if not 0 <= size < self._chunk_remaining:
            size = self._chunk_remaining
        if not size:
            return b''
        result = self._reader.read1(size)
        if not len(result):
            raise EOFError()
        self._chunk_remaining -= len(result)
        return result
    
    def readline(self, size=-1):
        if size < 0:
            size = inf
        result = bytearray()
        while size > 0 and not self._done:
            if not self._chunk_remaining:
                self._next_chunk()
                if self._done:
                    break
            limit = min(size, self._chunk_remaining, DEFAULT_BUFFER_SIZE)
            chunk = self._reader.readline(limit)
            self._chunk_remaining -= len(chunk)
            result.extend(chunk)
            if chunk.endswith(b'\n'):
                break
            if len(chunk) < limit:
                raise EOFError()
            size -= len(chunk)
        return bytes(result)
    
    def readinto(self, b):
        with memoryview(b) as b, b.cast('B') as b:
            offset = 0
            while offset < len(b) and not self._done:
                if not self._chunk_remaining:
                    self._next_chunk()
                    if self._done:
                        break
                with b[offset : offset + self._chunk_remaining] as subview:
                    result = self._reader.readinto(subview)
                    if result < len(subview):
                        raise EOFError()
                offset += result
                self._chunk_remaining -= result
        return result
    
    def readinto1(self, b):
        if not self._chunk_remaining and not self._done:
            self._next_chunk()
        with memoryview(b) as b, \
                b.cast('B') as b, \
                b[:self._chunk_remaining] as b:
            if not len(b):
                return 0
            result = self._reader.readinto1(b)
            if not result:
                raise EOFError()
        self._chunk_remaining -= result
        return result
    
    def _next_chunk(self):
        if self._chunk_seen:
            end = self._reader.read(2)
            assert end == b'\r\n'
        line = self._reader.readline()
        assert line.endswith(b'\r\n')
        self._chunk_remaining = int(line.split(b';', 1)[0], 16)
        self._chunk_seen = True
        
        self._done = self._chunk_remaining == 0
        if not self._done:
            return
        while True:
            line = self._reader.readline()
            if line == b'\r\n':
                break
            assert line.endswith(b'\r\n')

class RawDelegateWriter(RawIOBase):
    def writable(self):
        return True
    
    def writelines(lines):
        for line in lines:
            offset = 0
            with memoryview(line) as line, line.cast('B') as line:
                while offset < len(line):
                    with line[offset:] as slice:
                        offset += self.write(slice)
    
    def __init__(self, write):
        super().__init__()
        self.write = write

# Linux socket options:
TCP_CORK = 3
TCP_QUICKACK = 12

class HtmlTreeParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self._builder = TreeBuilder()
        self._open = list()
        
        # Avoid error about multiple top-level elements
        self._builder.start("", dict())
    
    def close(self):
        super().close()
        self._close_void()
        return self._builder.close()
    
    def handle_starttag(self, tag, attrs):
        self._close_void()
        d = dict()
        for [name, value] in attrs:
            if value is None:  # Empty HTML attribute, as in <input selected>
                value = ''
            d[name] = value
        self._builder.start(tag, d)
        self._open.append(tag)
    
    def handle_endtag(self, tag):
        if self._open and tag != self._open[-1] in self.VOID_ELEMENTS:
            self._builder.end(self._open.pop())
        try:
            closed = next(i
                for [i, open] in enumerate(reversed(self._open), 1)
                if open == tag)
            for i in range(closed):
                self._builder.end(self._open.pop())
        except StopIteration:
            msg = f'Spurious end tag </{tag}> at {repr(self.getpos())}'
            print(msg, file=sys.stderr)
    
    def handle_data(self, *pos, **kw):
        self._close_void()
        self._builder.data(*pos, **kw)
    
    def _close_void(self):
        if self._open and self._open[-1] in self.VOID_ELEMENTS:
            self._builder.end(self._open.pop())
    
    VOID_ELEMENTS = {'img', 'meta', 'link',
        'area', 'base', 'br', 'col', 'embed', 'hr', 'input', 'param',
        'source', 'track', 'wbr',
    }

def get_text(element):
    return ''.join(element.itertext())

if __name__ == "__main__":
    import clifunc
    clifunc.run()

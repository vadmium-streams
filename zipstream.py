#! /usr/bin/env python3

import sys
from struct import Struct
from streams import streamcopy, DelegateWriter
from binascii import crc32
from io import BufferedIOBase

FILE_SIG = b"PK\x03\x04"
FILE_STRUCT = Struct("< BB H H L LLL HH")
FLAGS_ENCRYPTED = 0
FLAGS_STREAMED = 3
FLAGS_STRONG_ENC = 6
FLAGS_UTF8 = 11
METHOD_STORE = 0

def iter_files(zip):
    while True:
        sig = zip.read(4)
        if not sig:
            break
        if len(sig) != 4 or not sig.startswith(b"PK"):
            raise ValueError("Invalid Zip record signature")
        if sig != FILE_SIG:
            break
        yield read_file_header(zip)

def read_file_header(zip):
    (_, _, flags, method, _, crc, comp, uncomp, namelen, extra) = (
        FILE_STRUCT.unpack(zip.read(FILE_STRUCT.size)))
    
    name = zip.read(namelen)
    if len(name) != namelen:
        raise EOFError("EOF in file name")
    encoding = "utf-8" if flags >> FLAGS_UTF8 & 1 else "cp437"
    name = name.decode(encoding)
    
    if flags >> FLAGS_STREAMED & 1:
        raise NotImplementedError("Data descriptor")
    return (flags, method, crc, comp, uncomp, name, extra)

def extract():
    zip = sys.stdin.buffer
    out = sys.stdout.buffer
    
    for [flags, method, crc, comp, uncomp, name, extra] in iter_files(zip):
        if name:
            print(name, file=sys.stderr)
        
        streamcopy(zip, DelegateWriter(), extra)
        
        skip = False
        if flags & (1 << FLAGS_ENCRYPTED | 1 << FLAGS_STRONG_ENC):
            print("Encrypted file", file=sys.stderr)
            skip = True
        if method != METHOD_STORE:
            print("Compression method {!r}".format(method), file=sys.stderr)
            skip = True
        if name.endswith((".txt", ".URL")):
            skip = True
        
        if skip:
            streamcopy(zip, DelegateWriter(), comp)
        else:
            crcwriter = Crc32Writer()
            streamcopy(zip, DelegateWriter(out, crcwriter), uncomp)
            if crcwriter.crc != crc:
                fmt = "Extracted CRC {:08X} != expected CRC {:08X}"
                print(fmt.format(crcwriter.crc, crc), file=sys.stderr)
            
            streamcopy(zip, DelegateWriter(), comp - uncomp)

class Crc32Writer(BufferedIOBase):
    def __init__(self):
        self.crc = 0
    def write(self, b):
        self.crc = crc32(b, self.crc)

if __name__ == "__main__":
    extract()

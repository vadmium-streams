#! /usr/bin/env python3

import os
import sys
import fuse
from contextlib import ExitStack
import rarfile
from bisect import bisect

def main():
    parser = fuse.ArgumentParser("rar [rarfs/]", 'rarfs')
    [rar, name, args] = parser.parse_args()
    self = Filesystem(name, args)
    
    with ExitStack() as cleanup:
        vol_cleanup = cleanup.enter_context(rarfile.Archive())
        vol_cleanup.open(rar)
        self.nodes = {fuse.ROOT_ID: empty_dir()}
        new_node = fuse.ROOT_ID + 1
        volumes = iter(vol_cleanup)
        self.size = 0
        self.namelen = 0
        while True:
            try:
                vol = next(volumes)
            except (StopIteration, FileNotFoundError):
                break
            for [type, flags, packed, block] in vol:
                if type != rarfile.FILE:
                    continue
                [path, unpacked, crc,
                    mtime, mtime_flags, mtime_frac, exttime] = block
                attrs = dict(size=unpacked,
                    mtime=rarfile.unpack_dostime(mtime),
                )
                if mtime_frac is not None:
                    [odd, attrs['mtimensec']] = rarfile.unpack_exttime_field(
                        mtime_flags, mtime_frac)
                    attrs['mtime'] += odd
                for [t, value] in zip('ca', exttime):
                    if value is None:
                        continue
                    time = int.from_bytes(value[:4], 'little')
                    attrs[t + 'time'] = rarfile.unpack_dostime(time)
                    [odd, attrs[t + 'timensec']] \
                        = rarfile.unpack_exttime_field(tflags, value[4:])
                    attrs[t + 'time'] += odd
                
                if flags & 0xE0 == 0xE0:
                    dir = self.nodes[fuse.ROOT_ID]
                    for dirname in path:
                        dir = self.nodes[dir['dir'][dirname]]
                    assert dir['type'] == fuse.Filesystem.DT_DIR
                    dir.update(attrs)
                    continue
                
                dir = self.nodes[fuse.ROOT_ID]
                for dirname in path[:-1]:
                    node = dir['dir'].setdefault(dirname, new_node)
                    if node is new_node:
                        dir['links'] += 1
                        self.nodes[new_node] = empty_dir()
                        self.namelen = max(self.namelen, len(dirname))
                        new_node += 1
                    dir = self.nodes[node]
                
                vol_pos = (vol_cleanup.volname, vol.vol.tell())
                if flags & rarfile.SPLIT_BEFORE:
                    try:
                        existing = self.nodes[dir['dir'][path[-1]]]
                    except LookupError:
                        continue
                    assert existing['size'] == unpacked
                    for t in 'mac':
                        t += 'time'
                        assert existing.get(t) == attrs.get(t)
                        t += 'nsec'
                        assert existing.get(t) == attrs.get(t)
                    existing['offsets'].append(existing['blocks'])
                    existing['vols'].append(vol_pos)
                    existing['blocks'] += packed
                    continue
                attrs['type'] = fuse.Filesystem.DT_REG
                attrs['blocks'] = packed
                attrs['offsets'] = [0]
                attrs['vols'] = [vol_pos]
                node = dir['dir'].setdefault(path[-1], new_node)
                assert node is new_node
                self.nodes[new_node] = attrs
                self.namelen = max(self.namelen, len(path[-1]))
                new_node += 1
            self.size += vol.vol.tell()
        for attrs in self.nodes.values():
            blocks = attrs.get('blocks')
            if blocks is not None:
                attrs['blocks'] = ceildiv(blocks, 512)
        
        self.make_dir()
        self.mount("rarfs", rar)
        cleanup.callback(self.close)
        
        while True:
            self.handle_request()

def empty_dir():
    return {'type': fuse.Filesystem.DT_DIR, 'links': 2, 'dir': dict()}

class Filesystem(fuse.Filesystem):
    def __init__(self):
        fuse.Filesystem.__init__(self)
        self.volname = None
    
    def close(self):
        if self.volname is not None:
            self.rar.close()
        fuse.Filesystem.close(self)
    
    def statfs(self):
        return dict(
            blocks=self.size,
            files=len(self.nodes),
            namelen=self.namelen,
        )
    
    def lookup(self, node, name):
        return self.nodes[node]['dir'][name.decode('ascii')]
    
    def getattr(fs, nodeid):
        return fs.nodes[nodeid]
    
    def readdir(self, nodeid, offset):
        dir = self.nodes[nodeid]['dir']
        for [i, [name, nodeid]] in enumerate(dir.items()):
            if i < offset:
                continue
            type = self.nodes[nodeid]['type']
            yield (nodeid, name.encode('ascii'), type, i + 1)
    
    def read(fs, nodeid, offset, size):
        result = bytearray()
        size = min(size, fs.nodes[nodeid]['size'] - offset)
        offsets = fs.nodes[nodeid]['offsets']
        i = bisect(offsets, offset) - 1
        while size:
            [rar, pos] = fs.nodes[nodeid]['vols'][i]
            if fs.volname != rar:
                if fs.volname is not None:
                    fs.rar.close()
                    fs.volname = None
                fs.rar = open(rar, 'rb')
                fs.volname = rar
            part_offset = offset - offsets[i]
            fs.rar.seek(pos + part_offset)
            i += 1
            if i < len(offsets):
                chunk = min(offsets[i] - part_offset, size)
            else:
                chunk = size
            data = fs.rar.read(chunk)
            assert len(data) == chunk
            result.extend(data)
            offset += chunk
            size -= chunk
        return result

def ceildiv(a, b):
    return -(-a // b)

if __name__ == '__main__':
    main()

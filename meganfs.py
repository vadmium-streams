#! /usr/bin/env python3

import subprocess
import sys
from http.client import HTTPSConnection, HTTP_PORT
from contextlib import ExitStack
from megan import Megan, NodeType, decrypt_name, base64_decode
from megan import make_request, decode_response, make_encryptor, split_key
from megan import bitmask, ceildiv
from http import HTTPStatus
from socket import create_connection, IPPROTO_TCP, TCP_NODELAY
import selectors
import fuse
import re
from errno import ENOENT
from base64 import urlsafe_b64encode
from datacopy import TerminalLog, Progress
from io import BytesIO

def main():
    parser = fuse.ArgumentParser("https://mega.nz/#key {mnt,''}", '')
    parser.add_argument('-v',
        action='store_true', help='verbose messages')
    [url, name, args] = parser.parse_args()
    self = Filesystem(name, args)
    self.dl = None
    self.v = args.v
    
    self.megan = Megan(url)
    url = re.sub('^[^:/]*:', '', url, 1)
    with ExitStack() as cleanup:
        cleanup.callback(self.close)
        
        self.api = HTTPSConnection('g.api.mega.co.nz')
        cleanup.callback(self.api.close)
        
        if self.megan.folder is None:
            req = self.megan.get_file_request()
            url = '!'.join(url.split('!', 2)[:2])
        else:
            [req, headers, body] = self.megan.get_folder_request()
            headers = dict(headers)
            self.api.request('POST', f'/{req}', headers=headers, body=body)
            if self.v:
                log = TerminalLog()
                log.write(f'POST {req} {body!r}: ')
            with ExitStack() as recv:
                resp = recv.enter_context(self.api.getresponse())
                size = int(resp.msg["Content-Length"])
                if self.v:
                    log.write(f'{resp.status} {resp.reason} {size} B\n')
                    recv.callback(Progress.close, log)
                    prog = Progress(log, size)
                data = bytearray()
                while len(data) < size:
                    chunk = resp.read(64*1024)
                    data.extend(chunk)
                    if self.v:
                        prog.update(len(data))
            self.megan.handle_folder_response(resp.msg, data)
            
            if self.megan.file is None:
                req = None
            else:
                req = self.megan.get_folder_file_request(self.megan.file)
                [self.megan.key, req] = req
            
            [url, frag] = url.split('#', 1)
            frag = frag.split("/", 1)
            if len(frag) > 1:
                url = f'{url}#/{frag[1]}'
        if req is None:
            self.dl_node = None
        else:
            [req, headers, body] = req
            headers = dict(headers)
            self.api.request('POST', f'/{req}', headers=headers, body=body)
            with self.api.getresponse() as resp:
                resp = self.megan.handle_file_response(resp.msg, resp.read())
            [self.K, self.b] = resp
            self.dl_node = fuse.ROOT_ID
            self.g = self.megan.resp['g']
            self.dl_size = self.megan.resp['s']
        if not self.name:
            self.name = self.megan.name
            print(self.name)
        
        if self.megan.folder is None or self.megan.file is not None:
            self.make_file()
        else:
            self.dirs = dict()
            for [handle, node] in self.megan.nodes.items():
                if handle == self.megan.root:
                    continue
                if node['parent'] == self.megan.root:
                    dir = fuse.ROOT_ID
                else:
                    dir = make_nodeid(node['parent'])
                self.dirs.setdefault(dir, list()).append((handle, node))
            self.make_dir()
        self.mount("meganfs", url)
        self.selector = selectors.DefaultSelector()
        cleanup.callback(self.selector.close)
        self.selector.register(self.fuse, selectors.EVENT_READ,
            self.handle_request)
        while True:
            for [key, events] in self.selector.select():
                try:
                    current = self.selector.get_key(key.fileobj)
                except KeyError:
                    continue
                if not current.events & key.events:
                    continue
                key.data()

class Filesystem(fuse.Filesystem):
    def close(self):
        try:
            if self.dl is not None:
                self.dl.close()
        finally:
            super().close()
    
    def handle_request(self):
        unique = fuse.Filesystem.handle_request(self)
        if unique is not None:
            self.read_unique = unique
            # Changing a selector's "events" mask to 0 raises ValueError
            self.selector.unregister(self.fuse)
    
    def statfs(self):
        result = {
            'bsize': 0x80000,
            'frsize': 16,
            'namelen': len(self.megan.name),
        }
        result['files'] = len(self.megan.nodes)
        result['blocks'] = 0
        for n in self.megan.nodes.values():
            result['blocks'] += ceildiv(n.get('size', 0), 16)
        return result
    
    def getattr(self, node):
        node = self.megan.nodes[self.decode_nodeid(node)]
        result = {
            'type': TYPE_MAP[node['type']],
            'size': node.get('size', 0),
        }
        if 'ts' in node:
            result['ctime'] = node['ts']
            result['mtime'] = node['ts']
        return result
    
    def lookup(self, dir, name):
        node = self.get_dir_map(dir).get(name)
        if node is None:
            raise OSError(ENOENT, None)
        [handle, node] = node
        return make_nodeid(handle)
    
    def readdir(self, dir, offset):
        dir = self.get_dir_map(dir).items()
        for [i, [name, [handle, node]]] in enumerate(dir):
            if i < offset:
                continue
            type = TYPE_MAP[node['type']]
            yield (make_nodeid(handle), name, type, i + 1)
    
    def get_dir_map(self, dir):
        if isinstance(self.dirs[dir], list):
            map = dict()
            
            keys = bytes().join(
                node['key'] for [handle, node] in self.dirs[dir])
            with make_encryptor('-aes-128-ecb', '-d', self.megan.key,
                    stdout=subprocess.PIPE) as dec:
                [keys, err] = dec.communicate(keys)
            assert dec.returncode == 0
            
            keys = BytesIO(keys)
            for [handle, node] in self.dirs[dir]:
                [k, b] = split_key(keys.read(len(node['key'])))
                name = decrypt_name(node['a'], k)
                map[name.encode('ascii')] = (handle, node)
            self.dirs[dir] = map
        return self.dirs[dir]
    
    def read(self, nodeid, offset, size):
        if nodeid != self.dl_node:
            node = self.decode_nodeid(nodeid)
            [key, req] = self.megan.get_folder_file_request(node)
            [req, headers, body] = req
            headers = dict(headers)
            self.api.request('POST', f'/{req}', headers=headers, body=body)
            with self.api.getresponse() as resp:
                resp = decode_response(resp.msg, resp.read())
            self.g = resp['g']
            [self.K, self.b] = split_key(key)
            self.dl_node = nodeid
            self.dl_size = resp['s']
            self.conn_offset = None
        if offset >= self.dl_size:
            return b''
        if self.dl is not None and self.conn_offset != offset:
            self.dl.close()
            self.dl = None
        if self.dl is None:
            assert self.g.startswith('http://')
            [host, path] = self.g[len('http://'):].split('/', 1)
            self.dl = create_connection((host, HTTP_PORT))
            self.dl.setsockopt(IPPROTO_TCP, TCP_NODELAY, 1)
            with self.dl.makefile('wb') as writer:
                writer.write(b'GET /')
                writer.write(path.encode('ascii'))
                writer.write(b' HTTP/1.1\r\n'
                    b'Host: ')
                writer.write(host.encode('ascii'))
                writer.write(b'\r\n'
                    b'Range: bytes=')
                writer.write(b'%d' % offset)
                writer.write(b'-\r\n'
                    b'User-Agent: meganfs\r\n'
                    b'\r\n')
                self.conn_offset = None
        
        self.pending_read = (offset, size)
        return self.start_recv()
    
    def start_recv(self):
        [self.offset, size] = self.pending_read
        self.padding = self.offset % 16
        self.buffer = bytearray(self.padding + size)
        if self.conn_offset:
            chunk = min(len(self.resp_buf), self.resp_len, size)
            self.resp_pos = self.padding + chunk
            self.buffer[self.padding:self.resp_pos] = self.resp_buf[:chunk]
            del self.resp_buf[:chunk]
            
            if chunk < min(self.resp_len, size):
                self.selector.register(self.dl, selectors.EVENT_READ, self.recv_body)
            else:
                return self.finish_read_sync()
        else:
            self.resp_buf = bytearray(0x10000)
            self.resp_pos = 0
            self.selector.register(self.dl, selectors.EVENT_READ, self.recv_header)
        return ...
    
    def recv_header(self):
        if self.v and not self.resp_pos:
            sys.stderr.write('Starting to receive HTTP response\n')
        searched = max(self.resp_pos - 3, 0)
        with memoryview(self.resp_buf) as view, view[self.resp_pos:] as view:
            self.resp_pos += self.dl.recv_into(view)
        end = self.resp_buf.find(b'\r\n\r\n', searched, self.resp_pos)
        if end < 0:
            assert self.resp_pos < len(self.resp_buf)
            return
        
        assert self.resp_buf.startswith(b'HTTP/1.1 ')
        p = len(b'HTTP/1.1 ')
        if int(self.resp_buf[p : p + 3]) != HTTPStatus.PARTIAL_CONTENT:
            msg = self.resp_buf[p : self.resp_buf.index(b'\r\n', p + 3)]
            raise Exception(msg.decode('ascii'))
        assert self.resp_buf.find(b'\r\nTransfer-Encoding:', p + 6, end) < 0
        
        start = b'\r\nContent-Length:'
        self.resp_len = self.resp_buf.find(start, p + 6, end)
        assert self.resp_len >= 0
        self.resp_len += len(start)
        len_end = self.resp_buf.find(b'\n', self.resp_len, end + 2)
        self.resp_len = int(self.resp_buf[self.resp_len:len_end])
        
        chunk = min(
            self.resp_pos - end - 4,
            len(self.buffer) - self.padding,
            self.resp_len,
        )
        filled = self.padding + chunk
        self.buffer[self.padding:filled] = self.resp_buf[end + 4 : end + 4 + chunk]
        self.resp_buf = self.resp_buf[end + 4 + chunk : self.resp_pos]
        self.resp_pos = filled
        if chunk < self.resp_len and filled < len(self.buffer):
            self.selector.modify(self.dl, selectors.EVENT_READ, self.recv_body)
            return
        
        self.finish_read()
    
    def recv_body(self):
        with memoryview(self.buffer) as view, \
                view[self.resp_pos:self.padding + self.resp_len] as view:
            self.resp_pos += self.dl.recv_into(view)
        if self.resp_pos < min(self.padding + self.resp_len, len(self.buffer)):
            return
        
        self.finish_read()
    
    def finish_read(self):
        self.selector.unregister(self.dl)
        self.reply(self.read_unique, self.finish_read_sync())
        self.selector.register(self.fuse, selectors.EVENT_READ,
            self.handle_request)
    
    def finish_read_sync(self):
        self.resp_len -= self.resp_pos - self.padding
        self.conn_offset = self.offset + self.resp_pos - self.padding
        iv = (self.b & ~0 << 64) + (self.offset // 16 & bitmask(128))
        del self.buffer[self.resp_pos:]
        with make_encryptor('-aes-128-ctr', '-d', self.K, iv,
                stdout=subprocess.PIPE) as decryptor:
            [buffer, err] = decryptor.communicate(self.buffer)
        assert decryptor.returncode == 0
        return buffer[self.padding:]
    
    def decode_nodeid(self, node):
        if node & 1 << 48:
            node &= bitmask(48)
            return urlsafe_b64encode(node.to_bytes(6, 'big')).decode('ascii')
        else:
            assert node == fuse.ROOT_ID
            return self.megan.root

def make_nodeid(handle):
    return 1 << 48 ^ int.from_bytes(base64_decode(handle, 48), 'big')

TYPE_MAP = {
    NodeType.FILE: fuse.Filesystem.DT_REG,
    NodeType.FOLDER: fuse.Filesystem.DT_DIR,
}

if __name__ == '__main__':
    with fuse.handle_termination():
        main()

#! /usr/bin/env python3

from io import SEEK_CUR, SEEK_END
import fuse
from contextlib import ExitStack
import zipstream
from errno import ENOENT, ENOSYS, ESPIPE
from shorthand import ceildiv
from zipfile import ZipInfo, ZipFile, ZIP_STORED, ZIP_DEFLATED
import zlib
from os import fsdecode, fsencode

def main():
    parser = fuse.ArgumentParser()
    parser.add_argument('--scan-files', action='store_true')
    [file, mnt, args] = parser.parse_args()
    fs = Filesystem(mnt, args)
    with ExitStack() as cleanup:
        fs.zip = cleanup.enter_context(open(file, 'rb'))
        fs.nodes = {fuse.ROOT_ID: empty_dir()}
        fs.namelen = 0
        if args.scan_files:
            for r in zipstream.iter_files(fs.zip):
                [flags, method, crc, comp, uncomp, name, extra] = r
                member = ZipInfo()
                
                METHODS = {
                    zipstream.METHOD_STORE: ZIP_STORED,
                    8: ZIP_DEFLATED,
                }
                member.compress_type = METHODS[method]
                
                member.compress_size = comp
                member.file_size = uncomp
                member.filename = name
                fs.add_member(member, fs.zip.tell() + extra)
                fs.zip.seek(+extra + comp, SEEK_CUR)
            fs.size = fs.zip.tell()
        else:
            fs.size = fs.zip.seek(0, SEEK_END)
            for member in ZipFile(fs.zip, 'r').infolist():
                fs.add_member(member)
        fs.decomp_node = None
        
        fs.make_dir()
        cleanup.callback(fs.close)
        fs.mount("zipfs", file)
        while True:
            fs.handle_request()

def empty_dir():
    return {'type': fuse.Filesystem.DT_DIR, 'links': 2, 'dir': dict()}

class Filesystem(fuse.Filesystem):
    def add_member(self, member, offset=None):
        name = member.filename.split('/')
        
        dir = self.nodes[fuse.ROOT_ID]
        for dirname in name[:-1]:
            new_node = fuse.ROOT_ID + len(self.nodes)
            node = dir['dir'].setdefault(dirname, new_node)
            if node is new_node:
                dir['links'] += 1
                self.nodes[new_node] = empty_dir()
                self.namelen = max(self.namelen, len(dirname))
            dir = self.nodes[node]
        
        if name[-1] > '':
            new_node = fuse.ROOT_ID + len(self.nodes)
            node = dir['dir'].setdefault(name[-1], new_node)
            assert node is new_node
            self.nodes[new_node] = {
                'type': Filesystem.DT_REG,
                'size': member.file_size,
                'offset': offset,
                'member': member,
                'blocks': ceildiv(member.compress_size, 512),
            }
            self.namelen = max(self.namelen, len(name[-1]))
    
    def statfs(self):
        return dict(
            blocks=self.size,
            files=len(self.nodes),
            namelen=self.namelen,
        )
    
    def getattr(self, node):
        return self.nodes[node]
    
    def lookup(self, node, name):
        node = self.nodes[node]['dir'].get(fsdecode(name))
        if node is None:
            raise OSError(ENOENT, None)
        return node
    
    def readdir(self, node, offset):
        dir = self.nodes[node]['dir']
        for [i, [name, node]] in enumerate(dir.items()):
            if i < offset:
                continue
            type = self.nodes[node]['type']
            yield (node, fsencode(name), type, i + 1)
    
    def read(self, node, offset, size):
        node = self.nodes[node]
        comp = node['member'].compress_size
        if node['member'].compress_type == ZIP_STORED:
            if offset >= comp:
                return b''
            return self.read_file(node, offset, min(size, comp - offset))
        if node['member'].compress_type == ZIP_DEFLATED:
            if offset == 0:
                self.decomp_node = node
                self.decomp_obj = zlib.decompressobj(-zlib.MAX_WBITS)
                self.decomp_offset = 0
                self.comp_offset = 0
                self.comp_left = comp
            elif node is not self.decomp_node \
                    or offset != self.decomp_offset:
                raise OSError(ESPIPE, None)
            
            result = b''
            while len(result) < size and not self.decomp_obj.eof:
                data = max(size, 0x10000) - len(self.decomp_obj.unconsumed_tail)
                data = min(data, self.comp_left)
                data = self.read_file(node, self.comp_offset, data)
                self.comp_left -= len(data)
                self.comp_offset += len(data)
                
                data = self.decomp_obj.unconsumed_tail + data
                decomp = self.decomp_obj.decompress(data, size - len(result))
                if not data and not decomp:
                    raise EOFError()
                result += decomp
            self.decomp_offset += len(result)
            return result
        raise OSError(ENOSYS, None)
    
    def read_file(self, node, offset, size):
        if node['offset'] is None:
            self.zip.seek(node['member'].header_offset)
            r = self.zip.read(4)
            assert r == zipstream.FILE_SIG
            r = zipstream.read_file_header(self.zip)
            [flags, method, crc, comp, uncomp, name, extra] = r
            node['offset'] = self.zip.tell() + extra
        self.zip.seek(node['offset'] + offset)
        return self.zip.read(size)

if __name__ == '__main__':
    with fuse.handle_termination():
        main()

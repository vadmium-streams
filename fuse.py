import subprocess
from stat import S_IFREG, S_IWRITE, S_ISREG, S_ISDIR
from stat import S_IXUSR, S_IXGRP, S_IXOTH, S_IROTH, S_IWOTH
import os
import errno
from struct import Struct
from socket import socketpair, SOCK_STREAM
from socket import CMSG_SPACE, MSG_CTRUNC, SOL_SOCKET, SCM_RIGHTS
from io import BytesIO
from functools import partial
import sys
import argparse
from enum import IntEnum
from signal import signal, SIGINT, SIGHUP, SIGTERM, SIGPWR, SIG_DFL
from contextlib import contextmanager

class ArgumentParser(argparse.ArgumentParser):
    def __init__(parser, args_usage=None, default_mnt=''):
        parser.auto_mnt = args_usage is None
        if parser.auto_mnt:
            args_usage = "fsname [{fsname.mnt,''}]"
        parser.default_mnt = default_mnt
        argparse.ArgumentParser.__init__(parser,
            prog=sys.argv[0] + " " + args_usage,
        )
        parser.add_argument('-o',
            action='append', default=[], dest='options', help='mount options')
        parser.add_argument('--trace-ops', action='store_true')
    
    def parse_args(self):
        if len(sys.argv) < 2:
            self.print_help()
            raise SystemExit()
        fsname = sys.argv[1]
        if len(sys.argv) > 2:
            name = sys.argv[2]
        else:
            name = self.default_mnt
        args = argparse.ArgumentParser.parse_args(self, sys.argv[3:])
        if self.auto_mnt and name == self.default_mnt:
            name = fsname + '.mnt'
        return (fsname, name, args)

class Opcode(IntEnum):
    LOOKUP = 1
    FORGET = 2
    GETATTR = 3
    SETATTR = 4
    READLINK = 5
    SYMLINK = 6
    MKNOD = 8
    MKDIR = 9
    UNLINK = 10
    RMDIR = 11
    RENAME = 12
    LINK = 13
    OPEN = 14
    READ = 15
    WRITE = 16
    STATFS = 17
    RELEASE = 18
    FSYNC = 20
    SETXATTR = 21
    GETXATTR = 22
    LISTXATTR = 23
    REMOVEXATTR = 24
    FLUSH = 25
    INIT = 26
    OPENDIR = 27
    READDIR = 28
    RELEASEDIR = 29
    FSYNCDIR = 30
    GETLK = 31
    SETLK = 32
    SETLKW = 33
    ACCESS = 34
    CREATE = 35
    INTERRUPT = 36
    BMAP = 37
    DESTROY = 38
    IOCTL = 39
    POLL = 40
    NOTIFY_REPLY = 41
    BATCH_FORGET = 42
    FALLOCATE = 43
    READDIRPLUS = 44
    RENAME2 = 46
    COPY_FILE_RANGE = 47
    SETUPMAPPING = 48
    REMOVEMAPPING = 49
    SYNCFS = 50
    TMPFILE = 51

class Filesystem:
    '''Fuse filesystem driver base class
    
    Methods to be implemented by a subclass:
    
    statfs(): Should return the following items in a dictionary:
        blocks: File system size in units of "frsize"
        frsize: Fundamental block size; defaults to 1
        files: Number of file, directory, etc nodes; defaults to 1
        bsize: Defaults to 1
        namelen: Maximum directory entry name length
    The "bfree", "bavail", and "ffree" fields of the "statfs" system call
    are set to zero.
    
    getattr(node, /): Should return the following items in a dictionary:
        size: Defaults to 0
        blocks: In 512 B units; default based on size
        atime, atimensec, mtime, mtimensec, ctime, ctimensec: Default to 0
        type: Filesystem.DT_* value, if "mode" not specified. Defaults to
            DT_REG, however if the mounted file system root is a directory,
            the root must be reported as a directory.
        mode: File type and permissions. Defaults to read and write
            permission for everyone, with execute permission added for
            directories.
        links: Number of hard links. Defaults to 1.
    The inode number is assumed to be the "node" value. The owner user and
    group id's are set to 0. The "rdev" field is set to 0.
    
    lookup(node, name, /): Return node id. for a directory entry
        The "node" argument identifies the directory in which to look up
    "name".
    
    open(node, flags, /): Return 2-tuple:
        * 64-bit file handle
        * Combination of the following bit masks
            * FOPEN_DIRECT_IO
            * FOPEN_KEEP_CACHE
            * FOPEN_NONSEEKABLE
    
    readdir(node, offset, /): Yield a 4-tuple for each entry in a directory:
        * Node id.
        * Entry name
        * Node type: one of the DT_* values
        * Offset to specify to skip this entry and jump to the next entry (or
            the end of the directory)
    An offset of zero is passed in to start from the first directory entry.
    
    read(node, offset, size, /)
    
    Node identifiers are 64-bit integers.
    '''
    
    def __init__(self, mnt, args):
        self.fuse = None
        self.remove_file = False
        self.remove_dir = False
        self.name = mnt
        self.args = args
    
    def make_file(self):
        try:
            os.mknod(self.name, S_IFREG | S_IWRITE)
            self.remove_file = True
        except FileExistsError:
            pass
    
    def make_dir(self):
        try:
            os.mkdir(self.name)
            self.remove_dir = True
        except FileExistsError:
            pass
    
    def mount(self, type, fsname):
        env = dict(os.environ)
        [recv, send] = socketpair(type=SOCK_STREAM)
        with recv:
            with send:
                send.set_inheritable(True)
                env["_FUSE_COMMFD"] = format(send.fileno())
                o = list(self.args.options)
                escapes = {ord("\\"): "\\" * 2, ord(","): "\\,"}
                o.append("fsname=" + fsname.translate(escapes))
                o.append("subtype=" + type)
                args = ("fusermount", "-o", ",".join(o), "--", self.name)
                status = subprocess.call(args, env=env, close_fds=False)
            if status != 0:
                raise SystemExit(status)
            self.mounted = True
            
            fd = Struct("i")
            msg = recv.recvmsg(1, CMSG_SPACE(fd.size))
            [data, ancdata, flags, address] = msg
            assert not flags & MSG_CTRUNC
            [msg] = ancdata
            [level, cmsg_type, data] = msg
            assert level == SOL_SOCKET and cmsg_type == SCM_RIGHTS
            [self.fuse] = fd.unpack_from(data)
    
    def close(self):
        if self.fuse is not None:
            os.close(self.fuse)
            if self.mounted:
                args = ("fusermount", "-u", '-z', "--", self.name)
                subprocess.check_call(args)
            self.fuse = None
        if self.remove_file:
            stat = os.lstat(self.name)
            if S_ISREG(stat.st_mode) and not stat.st_size:
                os.unlink(self.name)
        if self.remove_dir:
            os.rmdir(self.name)
    
    def handle_request(self):
        try:
            [opcode, unique, nodeid, reader] = get_request(self.fuse)
        except OSError as err:
            if err.errno != errno.ENODEV:
                raise
            self.mounted = False
            raise SystemExit()
        try:
            try:
                handler = self.handlers.get(opcode)
                if handler:
                    if self.args.trace_ops:
                        sys.stderr.write(f'{unique!r} {Opcode(opcode).name} {nodeid}')
                        if opcode not in {
                            Opcode.INIT, Opcode.LOOKUP,
                            Opcode.READ, Opcode.READDIR,
                        }:
                            sys.stderr.write('\n')
                else:
                    if opcode in Opcode.__members__.values():
                        sys.stderr.write('Unhandled opcode '
                            f'{Opcode(opcode).name} ({opcode})\n')
                    else:
                        sys.stderr.write(
                            "Unknown opcode {}\n".format(opcode))
                    handler = type(self).handle_unimplemented
                r = handler(self, nodeid, reader)
            except OSError as err:
                if err.errno is None:
                    raise
                self.reply_error(unique, err.errno)
                return None
        except Exception as exc:
            sys.excepthook(type(exc), exc, exc.__traceback__)
            self.reply_error(unique, errno.EIO)
            return None
        if r is Ellipsis:
            return unique
        if r is not None:
            self.reply(unique, r)
        return None
    
    handlers = dict()
    
    def handle_unimplemented(self, node, reader):
        raise OSError(errno.ENOSYS, None)
    handlers[Opcode.ACCESS] = handle_unimplemented
    handlers[Opcode.FLUSH] = handle_unimplemented
    handlers[Opcode.RELEASE] = handle_unimplemented
    handlers[Opcode.RELEASEDIR] = handle_unimplemented
    
    @partial(handlers.__setitem__, Opcode.INIT)
    def handle_init(self, nodeid, reader):
        [major, minor] = read_struct(reader, self.init_in)
        if self.args.trace_ops:
            sys.stderr.write(f' v{major}.{minor}')
        if major > 7:
            if self.args.trace_ops:
                sys.stderr.write(f' -> v7\n')
            return uint32.pack(7)
        assert major == 7 and minor >= 6
        [readahead, flags] = read_struct(reader, self.init_in_7_6)
        if self.args.trace_ops:
            sys.stderr.write(f' readahead {readahead}, flags 0x{minor:X}\n')
        
        ASYNC_READ = 1 << 0
        flags = 0
        write = 4096
        return self.init_out_7_6.pack(7, 6, readahead, flags, write)
    init_in = Struct("=LL")
    init_in_7_6 = Struct("=LL")
    init_out_7_6 = Struct("=LL LL 4x L")
    
    @partial(handlers.__setitem__, Opcode.LOOKUP)
    def handle_lookup(self, nodeid, reader):
        [target, _] = reader.read().split(b"\x00", 1)
        if self.args.trace_ops:
            sys.stderr.write(f'/{target!r}\n')
        nodeid = self.lookup(nodeid, target)
        r = self.entry_out.pack(nodeid, 0,
            self.cache_valid, self.cache_valid, 0, 0)
        return r + self.get_attrs(nodeid)
    entry_out = Struct("=QQQQLL")

    @partial(handlers.__setitem__, Opcode.FORGET)
    def handle_forget(fs, nodeid, reader):
        return None
    
    @partial(handlers.__setitem__, Opcode.STATFS)
    def handle_statfs(fs, nodeid, reader):
        result = fs.statfs()
        bfree = 0
        bavail = 0
        ffree = 0
        return fs.statfs_out.pack(result['blocks'], bfree, bavail,
            result.get('files', 1), ffree,
            result.get('bsize', 1), result['namelen'],
            result.get('frsize', 1),
        )
    statfs_out = Struct("=QQQ QQ LLL 28x")
    
    @partial(handlers.__setitem__, Opcode.GETATTR)
    def handle_getattr(fs, nodeid, reader):
        [flags, fh] = read_struct(reader, fs.getattr_in)
        return fs.attr_out.pack(fs.cache_valid, 0) + fs.get_attrs(nodeid)
    getattr_in = Struct("=L 4x Q")
    attr_out = Struct("=QL 4x")
    cache_valid = (1 << 64) - 1
    
    def get_attrs(self, nodeid):
        result = self.getattr(nodeid)
        
        type = result.get('type', Filesystem.DT_REG)
        mode = result.get('mode')
        if mode is None:
            mode = type << IF_SHIFT \
                | (S_IROTH | S_IWOTH) * (S_IXUSR | S_IXGRP | S_IXOTH)
            if S_ISDIR(mode):
                mode |= S_IXUSR | S_IXGRP | S_IXOTH
        
        inode = nodeid
        size = result.get('size', 0)
        blocks = result.get('blocks')
        if blocks is None:
            blocks = ceildiv(size, 512)
        uid = gid = 0
        rdev = 0
        return self.attr.pack(inode, size, blocks,
            result.get('atime', 0), result.get('mtime', 0),
            result.get('ctime', 0),
            result.get('atimensec', 0), result.get('mtimensec', 0),
            result.get('ctimensec', 0),
            mode,
            result.get('links', 1),
            uid, gid, rdev)
    attr = Struct("=QQQQQQLLLLLLLL")
    
    @partial(handlers.__setitem__, Opcode.OPEN)
    def handle_open(self, nodeid, reader):
        [flags] = read_struct(reader, uint32)
        return self.open_out.pack(*self.open(nodeid, flags))
    
    def open(self, nodeid, flags):
        return (0, self.FOPEN_KEEP_CACHE)
    
    @partial(handlers.__setitem__, Opcode.OPENDIR)
    def handle_opendir(fs, nodeid, reader):
        [flags] = read_struct(reader, uint32)
        return fs.open_out.pack(0, fs.FOPEN_KEEP_CACHE)
    
    open_out = Struct("=QL 4x")
    FOPEN_DIRECT_IO = 1 << 0
    FOPEN_KEEP_CACHE = 1 << 1
    FOPEN_NONSEEKABLE = 1 << 2
    
    @partial(handlers.__setitem__, Opcode.READ)
    def handle_read(self, nodeid, reader):
        [fh, offset, size, read_flags, lock_owner, flags] = read_struct(
            reader, self.read_in)
        if self.args.trace_ops:
            sys.stderr.write(' +{:,}; {:,} B\n'.format(offset, size))
        return self.read(nodeid, offset, size)
    
    @partial(handlers.__setitem__, Opcode.READDIR)
    def handle_readdir(self, nodeid, reader):
        [fh, offset, size, read_flags, lock_owner, flags] = read_struct(
            reader, self.read_in)
        if self.args.trace_ops:
            sys.stderr.write(f' +{offset:,}; {size:,} B\n')
        r = bytearray()
        for [nodeid, name, type, offset] in self.readdir(nodeid, offset):
            r.extend(self.dirent_header.pack(nodeid, offset, len(name), type))
            r.extend(name)
            r.extend(bytes(-len(name) % 8))
            if len(r) > size:
                del r[size:]
                break
        return r
    DT_FIFO = 1
    DT_CHR = 2
    DT_DIR = 4
    DT_BLK = 6
    DT_REG = 8
    DT_LNK = 10
    DT_SOCK = 0o14
    dirent_header = Struct("=QQLL")
    
    read_in = Struct("=QQLLQL")
    
    def reply(self, unique, reply):
        if self.args.trace_ops:
            sys.stderr.write(f'{unique} -> {len(reply):,} B\n')
        reply = self.out_header.pack(self.out_header.size + len(reply), 0, unique) + reply
        size = os.write(self.fuse, reply)
        assert size == len(reply)
    
    def reply_error(self, unique, error):
        if self.args.trace_ops:
            sys.stderr.write(f'{unique} -> {errno.errorcode[error]}\n')
        size = os.write(self.fuse, self.out_header.pack(self.out_header.size, -error, unique))
        assert size == self.out_header.size
    
    out_header = Struct("=LlQ")

def get_request(fuse):
    request = os.read(fuse, 8192)
    reader = BytesIO(request)
    h = read_struct(reader, in_header)
    [req_len, opcode, unique, nodeid, uid, gid, pid] = h
    assert req_len == len(request)
    return (opcode, unique, nodeid, reader)
in_header = Struct("=LLQQLLL 4x")

ROOT_ID = 1

MAX_SIZE = (1 << 63) - 1
IF_SHIFT = 12

@contextmanager
def handle_termination():
    for signalnum in (SIGHUP, SIGINT, SIGTERM, SIGPWR):
        signal(signalnum, termination_handler)
    try:
        yield
    except Termination as term:
        term.exec()

def termination_handler(signalnum, frame):
    raise Termination(signalnum)

class Termination(BaseException):
    def __init__(self, signalnum):
        self.signalnum = signalnum
    
    def exec(self):
        signal(self.signalnum, SIG_DFL)
        os.kill(os.getpid(), self.signalnum)

def read_struct(reader, struct):
    return struct.unpack(reader.read(struct.size))

uint32 = Struct("=L")

def ceildiv(a, b):
    return -(-a // b)

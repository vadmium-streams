#! /bin/python3

from net import request_decoded
from contextlib import ExitStack
import json
from urllib.parse import urlsplit, parse_qs, urlencode, urljoin
from sys import stderr
from datetime import timedelta, datetime, timezone
import re
from math import inf, fmod
from time import strftime, gmtime
import javascript
from io import TextIOWrapper
from functools import partial

def main(yt, client='TVHTML5_SIMPLY_EMBEDDED_PLAYER', version=None, kbps=inf,
        *, cache=False, audio=False, separate=False, qv=100):
    CLIENTS = {
        'TVHTML5_SIMPLY_EMBEDDED_PLAYER': "2.0",
        'ANDROID': "17.36.4",
        'TVLITE': '2',
    }
    if version is None:
        version = CLIENTS[client]
    
    key = 'AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8'
    client_obj = {
        "clientName": client,
        "clientVersion": version
    }
    if client == 'ANDROID':
        agent = f'com.google.android.youtube/{version}'
        client_obj["androidSdkVersion"] = 31
    else:
        agent = 'yt'
    
    split = urlsplit(yt)
    if split.netloc:
        if split.netloc == 'youtu.be':
            yt = split.path[1:]
        else:
            assert ('.' + split.netloc).endswith('.youtube.com')
            if split.path in {'/watch', '/watch_popup'}:
                [yt] = parse_qs(split.query)['v']
            else:
                assert split.path.startswith((
                    '/shorts/', '/live/', '/v/', '/embed/'))
                yt = split.path.split('/', 2)[2]
        assert split.scheme in {'http', 'https'}
    assert re.fullmatch(r'[\w-]{11}', yt, re.ASCII)
    
    with ExitStack() as cleanup:
        [header, yt] = request_decoded(
            'https://www.youtube.com/youtubei/v1/player',
            headers=(
                ('User-Agent', agent),
                ('Content-Type', 'application/json'),
                ('X-Goog-Api-Key', key),
            ),
            data=json.dumps({
                "context": {
                    "client": client_obj,
                    "thirdParty": {"embedUrl": "https://www.youtube.com"}},
                "videoId": yt}).encode('ascii'),
            types=('application/json',),
            cleanup=cleanup, cache=cache)
        yt = json.load(yt)
    
    if 'videoDetails' in yt:
        d = yt['videoDetails']
        if 'title' in d:
            stderr.write(f'{d["title"]}\n')
        if 'shortDescription' in d:
            stderr.write(f'{d["shortDescription"]}\n')
        length = timedelta(seconds=int(d["lengthSeconds"]))
        stderr.write(f'{length}')
        if 'viewCount' in d:
            stderr.write(f', {d["viewCount"]} views, author {d["author"]}')
        for attr in ('isPrivate', 'isUnpluggedCorpus', 'isLiveContent'):
            if d[attr]:
                stderr.write(f', {attr}')
        for attr in ('isCrawlable', 'allowRatings'):
            if not d[attr]:
                stderr.write(f', not {attr}')
        stderr.write('\n')
    
    s = yt['playabilityStatus']
    if s['status'] != 'OK':
        stderr.write(f'{s["status"]}: ')
        if 'reasonTitle' in s:
            stderr.write(f'{s["reasonTitle"]}\n')
        if 'reason' in s:
            stderr.write(f'{s["reason"]}\n')
        if 'messages' in s:
            [t] = s['messages']
            stderr.write(f'{t}\n')
        if 'reasonTitle' not in s and 'errorScreen' in s \
                and 'playerErrorMessageRenderer' in s['errorScreen']:
            s = s['errorScreen']['playerErrorMessageRenderer']
            for field in ('reason', 'subreason'):
                if field in s:
                    try:
                        t = ''.join(t['text'] for t in s[field]['runs'])
                    except LookupError:
                        t = s[field]['simpleText']
                    stderr.write(f'{t}\n')
            if 'learnMore' in s:
                [t] = s['learnMore']['runs']
                t = t['navigationEndpoint']['urlEndpoint']['url']
                stderr.write(f'{t}\n')
        if 'streamingData' not in yt:
            raise SystemExit(1)
    yt = yt['streamingData']
    
    mod_min = +inf
    mod_max = -inf
    for formats in ('formats', 'adaptiveFormats'):
        for format in yt.get(formats, ()):
            if 'lastModified' not in format:
                continue
            mod_min = min(mod_min, int(format['lastModified']))
            mod_max = max(mod_max, int(format['lastModified']))
    if mod_min <= mod_max:
        stderr.write(f'{format_time(mod_min)} to {format_time(mod_max)}\n')
    
    e = timedelta(seconds=float(yt['expiresInSeconds']))
    t = (datetime.now(timezone.utc) + e).astimezone()
    print(f"URL expiry in {e} ({t.isoformat(' ', 'minutes')})", file=stderr)
    
    if audio or separate:
        formats = 'adaptiveFormats'
    else:
        formats = 'formats'
    if not separate and formats not in yt and 'hlsManifestUrl' in yt:
        print(yt['hlsManifestUrl'])
        return
    yt = yt[formats]
    types = dict()
    for format in yt:
        [type, subtype] = format['mimeType'].split('/', 1)
        types.setdefault(type, list()).append(format)
        CODECS = r' *[^/]+/[^ ;]+ *; *codecs *= *"([^"\\]+)" *'
        codec = re.fullmatch(CODECS, format['mimeType']).group(1)
        best = 0
        for codec in codec.split(','):
            [codec, sep, rest] = codec.strip().partition('.')
            BEST_kbps = {
                'mp4v': 1600, 'avc1': 900, 'vp9': 600, 'av01': 400,
                'mp4a': 140, 'opus': 130,
            }
            best += BEST_kbps[codec]
        try:
            bitrate = format['bitrate'] / 1000
        except LookupError:
            bitrate = int(format['contentLength']) * 8
            bitrate /= int(format['approxDurationMs'])
        format['qv'] = bitrate / best
    
    qv = float(qv) / 100
    if not audio:
        print(best_url(types['video'], limit=float(kbps) * 1e3, qv=qv,
            cache=cache, agent=agent))
    if audio or separate:
        print(best_url(types['audio'], limit=float(kbps) * 1e3, qv=qv,
            cache=cache, agent=agent))

def best_url(yt, limit, *, qv, cache, agent):
    url = None
    def key(s):
        try:
            abr = s['averageBitrate']
        except LookupError:
            try:
                abr = int(s['contentLength']) * 8
                abr /= int(s['approxDurationMs']) * 1e-3
            except LookupError:
                abr = s['bitrate']
        # First, prioritize streams within the bitrate limit, followed by
        # each stream with the lowest bitrate exceeding that limit. Then,
        # prioritize streams that meet the minimum quality requirement,
        # followed by each stream with the highest quality under that
        # minimum. Finally, prioritize each stream with the lowest bitrate.
        return (+max(abr, limit), -min(s['qv'], qv), +abr)
    yt = sorted(yt, key=key)
    for s in yt:
        stderr.write(f'{s["itag"]} {s["quality"]}: {s["mimeType"]} ')
        try:
            try:
                abr = s["averageBitrate"] / 1000
            except LookupError:
                abr = int(s['contentLength']) * 8
                abr /= int(s['approxDurationMs'])
            stderr.write(f'{abr:.0f} ')
            if 'bitrate' in s:
                stderr.write('< ')
        except LookupError:
            pass
        if 'bitrate' in s:
            stderr.write(f'{s["bitrate"] / 1000:.0f} ')
        stderr.write(f'kb/s {s["qv"]:.1%}\n')
        
        if url is None:
            if len(yt) > 1:
                stderr.write('^^^\n')
            url = s['url']
    
    url = urlsplit(url)
    query = parse_qs(url.query,
        strict_parsing=True, keep_blank_values=True,
        encoding='ascii', errors='error')
    kw = dict(
        headers=(
            ('User-Agent', agent),
        ),
        types=('text/javascript',),
        cache=cache,
    )
    values = query.get('n', ())
    for [i, n] in enumerate(values):
        api = 'https://www.youtube.com/iframe_api'
        with ExitStack() as cleanup:
            [header, player] = request_decoded(api, **kw, cleanup=cleanup)
            player = TextIOWrapper(player, header.get_content_charset())
            [labels, [stmt, [[name, player]]]] = next(javascript.parse(player))
        assert stmt == 'var'
        assert name == 'scriptUrl' and isinstance(player, str)
        player = urljoin(api, player)
        
        with ExitStack() as cleanup:
            [header, player] = request_decoded(
                urljoin(player, '../player_ias.vflset/en_US/base.js'),
                **kw, cleanup=cleanup)
            player = TextIOWrapper(player, header.get_content_charset())
            found = None
            
            # Find top-level statements that are immediately invoked function
            # expressions: (function . . .(. . .) {. . .})(. . .)
            for [labels, stmt] in javascript.parse(player):
                if not isinstance(stmt, tuple) or stmt[0] != 'call' \
                        or not isinstance(stmt[1], tuple) \
                        or stmt[1][0] != 'function':
                    continue
                [type, name, params, body] = stmt[1]
                
                # In the function body, find statements assigning a
                # function expression to a variable: x = function . . .
                for [labels, stmt] in body:
                    if not isinstance(stmt, tuple) or stmt[0] != '=' \
                            or not isinstance(stmt[2], tuple) \
                            or stmt[2][0] != 'function':
                        continue
                    [type, name, params, body] = stmt[2]
                    
                    # See if the function includes the landmark
                    for expr in walk_block_exprs(body):
                        if isinstance(expr, str) \
                                and expr.startswith('enhanced_except'):
                            break
                    else:
                        continue
                    if found:
                        raise ValueError('Multiple functions found')
                    found = stmt[2]
        from javascript import dump; dump.dump_function(found, level=0); print()
        nn = call_func(found, n)
        print(n, '->', nn, file=stderr)
        values[i] = nn
    url = url._replace(query=urlencode(query, doseq=True, safe='/'))
    return url.geturl()

def eval(expr, scope):
    if isinstance(expr, tuple):
        if expr[0] == 'call':
            [op, expr, args] = expr
            return call(eval(expr, scope), *(eval(a, scope) for a in args))
        if expr[0] == 'property':
            [op, target, attr] = expr
            target = eval(target, scope)
            attr = eval(attr, scope)
            if isinstance(target, str):
                assert attr in {'split', 'slice'}
                return (attr, target)
            assert isinstance(target, list)
            if attr == 'length':
                return float(len(target))
            if attr == 'splice':
                return (attr, target)
            assert isinstance(attr, float) and attr in range(len(target))
            return target[int(attr)]
        if expr[0] == 'function':
            return expr
        if expr[0] == '=':
            [op, target, result] = expr
            
            assert isinstance(target, tuple)
            if target[0] == 'property':
                [expr, target, attr] = target
                target = eval(target, scope)
                attr = eval(attr, scope)
                
                result = eval(result, scope)
                assert isinstance(target, list)
                assert isinstance(attr, float) and attr in range(len(target))
                target[int(attr)] = result
            else:
                [expr, target] = target
                assert expr == 'identifier'
                result = eval(result, scope)
                assert target in scope
                scope[target] = result
            return result
        if expr[0] in {',', '<=', '!=', '%', '+'}:
            [op, a, b] = expr
            a = eval(a, scope)
            b = eval(b, scope)
            if op == ',':
                return b
            assert isinstance(a, float) and isinstance(b, float)
            if op == '<=':
                return a <= b
            if op == '!=':
                return a != b
            if op == '%':
                return fmod(a, b)
            assert op == '+'
            return a + b
        if expr[0] in {'&&', '||'}:
            [op, result, expr] = expr
            result = eval(result, scope)
            assert isinstance(result, bool)
            if result == (op == '&&'):
                result = eval(expr, scope)
            return result
        [op, expr] = expr
        if op == 'identifier':
            return scope[expr]
        assert op == 'prefix -'
        expr = eval(expr, scope)
        assert isinstance(expr, float)
        return -expr
    if isinstance(expr, (str, float)) or expr is None:
        return expr
    assert isinstance(expr, list)
    result = list()
    for expr in expr:
        assert expr != ('undefined',)
        expr = eval(expr, scope)
        result.append(expr)
    return result

def call(func, *args):
    if func[0] == 'function':
        return call_func(func, *args)
    [func, obj] = func
    if isinstance(obj, str):
        if func == 'slice':
            [start, end] = args
            assert isinstance(start, float) and start.is_integer() and start >= 0
            assert isinstance(end, float) and end.is_integer() and end >= start
            return obj[int(start):int(end)]
        assert func == 'split'
        [sep] = args
        assert sep == ''
        return list(obj)
    assert isinstance(obj, list) and func == 'splice'
    [start, delete, *args] = args
    assert isinstance(start, float) and start in range(len(obj))
    start = int(start)
    assert isinstance(delete, float) and delete in range(len(obj) - start)
    result = obj[start : start + int(delete)]
    obj[start : start + int(delete)] = args
    return result

def call_func(func, *args):
    [type, name, params, body] = func
    assert len(args) == len(params)
    scope = {name: arg for [name, arg] in zip(params, args)}
    exec_block(body, scope)
    return ('undefined',)

def exec_block(block, scope):
    for [labels, stmt] in block:
        if isinstance(stmt, tuple):
            if stmt[0] == 'var':
                [type, stmt] = stmt
                for [name, stmt] in stmt:
                    if stmt != ('undefined',):
                        scope[name] = eval(stmt, scope)
            elif stmt[0] == 'try':
                [type, block, catch, handler, final] = stmt
                exec_block(block, scope)
                exec_block(final, scope)
            else:
                eval(stmt, scope)
        else:
            eval(stmt, scope)

def walk_block_exprs(block):
    for stmt in block:
        yield from walk_stmt_exprs(stmt)

def walk_stmt_exprs(stmt):
    [labels, stmt] = stmt
    yield from walk_exprs(stmt)

def walk_exprs(expr):
    if expr == ('undefined',):
        return
    try:
        yield expr
    except StopIteration:
        return
    
    if isinstance(expr, tuple):
        if expr[0] in {'return', 'throw', 'property', '?'} \
                or expr[0].startswith(('prefix ', 'postfix')) \
                or expr[0] in javascript.OP_PRECEDENCE:
            for expr in expr[1:]:
                yield from walk_exprs(expr)
        elif expr[0] in {'var', 'object'}:
            [op, expr] = expr
            for [name, expr] in expr:
                yield from walk_exprs(expr)
        elif expr[0] == 'for':
            [op, init, cond, inc, body] = expr
            yield from walk_exprs(init)
            yield from walk_exprs(cond)
            yield from walk_exprs(inc)
            yield from walk_stmt_exprs(body)
        elif expr[0] == 'block':
            [op, body] = expr
            yield from walk_block_exprs(body)
        elif expr[0] == 'if':
            [op, cond, body, alt] = expr
            yield from walk_exprs(cond)
            yield from walk_stmt_exprs(body)
            yield from walk_stmt_exprs(alt)
        elif expr[0] in {'call', 'new'}:
            [op, expr, args] = expr
            yield from walk_exprs(expr)
            for expr in args:
                yield from walk_exprs(expr)
        elif expr[0] == 'for in':
            [op, name, expr, body] = expr
            yield from walk_exprs(name)
            yield from walk_exprs(expr)
            yield from walk_stmt_exprs(body)
        elif expr[0] == 'try':
            [op, body, catch, handler, final] = expr
            yield from walk_block_exprs(body)
            yield from walk_block_exprs(handler)
            yield from walk_block_exprs(final)
        elif expr[0] == 'do':
            [op, body, cond] = expr
            yield from walk_stmt_exprs(body)
            yield from walk_exprs(cond)
        elif expr[0] == 'switch':
            [op, expr, body] = expr
            yield from walk_exprs(expr)
            for [expr, body] in body:
                yield from walk_exprs(expr)
                yield from walk_block_exprs(body)
        else:
            assert expr[0] in {
                'function', 'identifier', 'this', 'regex',
                'break', 'continue',
            }
    elif isinstance(expr, list):
        for expr in expr:
            yield from walk_exprs(expr)
    else:
        assert isinstance(expr, (float, str, bool)) or expr is None

def format_time(us):
    [secs, us] = divmod(us, 10**6)
    return f'{strftime("%Y-%m-%d %H:%M:%S", gmtime(secs))}.{us:06d}Z'

if __name__ == '__main__':
    from clifunc import run
    run(main)

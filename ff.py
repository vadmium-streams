#! /usr/bin/env python3

import os, os.path
import sys
from configparser import RawConfigParser
import sqlite3
from tempfile import NamedTemporaryFile
from shutil import copyfileobj
from contextlib import contextmanager
import datetime
import subprocess
import json

def main(profile=None, domain=None, name=None):
    if profile is None:
        profile = get_default_profile()
    
    def key(cookie):
        host = tuple(reversed(cookie['host'].rstrip('.').split('.')))
        return (host, cookie['path'])
    
    if domain is not None:
        result = list()
    
    with open(f'{profile}/cookies.sqlite', 'rb') as orig, \
            TemporaryCopy(orig, prefix='cookies.', suffix='.sqlite') as copy:
        orig.close()
        con = sqlite3.connect(copy)
        con.row_factory = sqlite3.Row
        cur = con.execute('SELECT * FROM moz_cookies')
        for row in sorted(cur, key=key):
            assert row['inBrowserElement'] == 0
            assert row['isSecure'] in {0, 1}
            assert row['isHttpOnly'] in {0, 1}
            assert row['sameSite'] in range(3)
            if domain is not None:
                filter_cookie(result, domain, name, row, row['name'])
                continue
            EPOCH = datetime.datetime(1970, 1, 1)
            if row['expiry'] > (datetime.datetime.max - EPOCH) // datetime.timedelta(seconds=1):
                print(end=f'exp>{datetime.MAXYEAR}               ')
            else:
                print(end=f'exp {EPOCH + datetime.timedelta(seconds=row["expiry"])!s}')
            print(end=f' axx {(EPOCH + datetime.timedelta(microseconds=row["lastAccessed"])).isoformat(" ", "microseconds")} cr {(EPOCH + datetime.timedelta(microseconds=row["creationTime"])).isoformat(" ", "microseconds")} ')
            if row['isSecure']:
                print(end='https:')
            else:
                print(end='      ')
            print(end=f'//{row["host"]}{row["path"]} {row["name"]}={row["value"]}')
            if row['isHttpOnly']:
                print(end=' HttpOnly')
            if row['sameSite'] or row['rawSameSite']:
                print(end=f' SameSite={row["sameSite"]}')
                if row['rawSameSite'] != row['sameSite']:
                    print(end=f' (raw {row["rawSameSite"]})')
            if row['originAttributes'] > '':
                print(end=f" originAttributes {row['originAttributes']}")
            print()
    
    try:
        session = open(f'{profile}/sessionstore.jsonlz4', 'rb')
    except FileNotFoundError:
        session = open(f'{profile}/sessionstore-backups/recovery.jsonlz4',
            'rb')
    
    with session:
        magic = session.read(8)
        assert magic == b'mozLz40\x00'
        uncomp = session.read(4)
        assert len(uncomp) == 4
        uncomp = int.from_bytes(uncomp, 'little')
        MiB = 1 << 20
        assert uncomp <= 8*MiB
        session = session.read()
    LEGACY_FRAME = 0x184C2102 . to_bytes(4, 'little')
    session = LEGACY_FRAME + len(session).to_bytes(4, 'little') + session
    session = subprocess.check_output(('lz4', '-d'), input=session)
    assert len(session) == uncomp
    
    for cookie in sorted(json.loads(session)['cookies'], key=key):
        partitionKey = cookie['originAttributes'].pop('partitionKey', '')
        assert cookie['originAttributes'] == {
            'firstPartyDomain': '',
            'geckoViewSessionContextId': '',
            'inIsolatedMozBrowser': False,
            'privateBrowsingId': 0,
            'userContextId': 0,
        }
        assert cookie.get('secure', True) is True
        assert cookie.get('httponly', True) is True
        assert cookie.get('sameSite', 1) in {1, 2}
        found_name = cookie.get("name", "")
        if domain is not None:
            filter_cookie(result, domain, name, cookie, found_name)
            continue
        if 'secure' in cookie:
            print(end='https:')
        else:
            print(end='      ')
        print(end=f'//{cookie["host"]}{cookie["path"]} {found_name}={cookie["value"]}')
        if 'httponly' in cookie:
            print(end=' httponly')
        if 'sameSite' in cookie:
            print(end=f' sameSite={cookie["sameSite"]}')
        if partitionKey > '':
            print(end=f' partitionKey={partitionKey}')
        print()
    
    if domain is not None:
        if name is not None:
            if not result:
                raise LookupError(f'Cookie {name!r} not found')
        print(' '.join(result))

def filter_cookie(result, domain, name, cookie, found_name):
    if cookie['host'].startswith('.'):
        if not f'.{domain.lower()}'.endswith(cookie['host']):
            return
    else:
        if cookie['host'] != domain.lower():
            return
    if name is None:
        result.append(f'{found_name}={cookie["value"]}')
    elif name == found_name:
        if result:
            raise LookupError('Multiple matching entries')
        result.append(cookie['value'])

def get_default_profile():
    if sys.platform == 'win32':
        dir = f"{os.environ['APPDATA']}\\Mozilla\\Firefox"
    else:
        dir = f"{os.environ['HOME']}/.mozilla/firefox"
    
    parser = RawConfigParser(delimiters=('=',), comment_prefixes=())
    with open(f'{dir}/profiles.ini', 'rt', encoding='ascii') as file:
        parser.read_file(file)
    
    profile = None
    profiles = 0
    for section in parser.sections():
        if section.startswith('Install'):
            assert profile is None
            profile = parser.get(section, 'Default')
        profiles += section.startswith('Profile')
    if profile is not None:
        return os.path.join(dir, profile)
    
    default = None
    for i in range(profiles):
        if parser.has_option(f'Profile{i}', 'Default'):
            assert parser.get(f'Profile{i}', 'Default') == '1'
            assert not default
            default = i
    
    if parser.get(f'Profile{default}', 'IsRelative') == '1':
        return os.path.join(dir, parser.get(f'Profile{default}', 'Path'))
    else:
        assert parser.get(f'Profile{default}', 'IsRelative') == '0'
        return parser.get(f'Profile{default}', 'Path')

@contextmanager
def TemporaryCopy(orig, *pos, **kw):
    if sys.platform == 'win32':
        with NamedTemporaryFile(*pos, **kw, delete=False) as temp:
            copyfileobj(orig, temp)
            temp = temp.name
        try:
            yield temp
        finally:
            os.unlink(temp)
    else:
        with NamedTemporaryFile(*pos, **kw) as temp:
            copyfileobj(orig, temp)
            yield temp.name

if __name__ == '__main__':
    main(*sys.argv[1:])
